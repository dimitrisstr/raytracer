# Raytracer
The website of the project is available at https://dimitrisstr.gitlab.io/raytracer

## Features

- [x] Sphere Shape
- [x] Plane Shape
- [x] Cube Shape
- [x] Cylinder Shape
- [ ] Triangle Shape
- [ ] Groups
- [ ] Bounding Boxes 
- [x] Stripes Pattern
- [x] Ring Pattern
- [x] Checkers Pattern
- [x] Gradient Pattern
- [ ] Perturb Pattern
- [x] Reflection
- [x] Refraction
- [x] YAML Scene Parser
- [ ] Obj Parser
- [x] Gitlab page
***

## Yaml Scene Syntax

### Add a camera
```yaml
- add: camera
  width: unsigned int
  height: unsigned int
  field_of_view: radians
  from: [x, y, z]               # The position of the camera.
  to: [x, y, z]                 # The direction of the camera.
  up: [x, y, z]                 # The up direction of the camera.
```

### Add a light
```yaml
- add: light
  at: [x, y, z]                 # Light direction.
  intensity: [r, g, b]          # RGB values between 0 and 1
```

### Add a shape
```yaml
- add: sphere | plane | cube | cylinder
  # Cylinder properties
  min: float
  max: float
  closed: bool
  transform:                    # Optional field. Transform section.
    # ...
  material:                     # Optional field. Material section.
    # ...
  pattern:                      # Optional field. Pattern section.
    # ...
```

### Transform Section
```yaml
transform:
  # The transform section may have one or more transformations.
  - [scale, x, y, z]
  - [rotate_x, radians]
  - [rotate_y, radians]
  - [rotate_z, radians]
  - [translate, x, y, z]
  - [shearing, xy, xz, yx, yz, zx, zy]
```

### Material Section
```yaml
material:
  ambient: float
  diffuse: float
  specular: float  
  shininess: float
  reflective: float
  transparency: float
  refractive_index: float
```
#### Pattern
```yaml
pattern:
  type: stripes | checkers | ring | gradient
  transform:                   # Transform section.
    # ...
  pattern1:
    color: [r, g, b]          # The range of rgb values is [0, 1]
  pattern2:
    color: [r, g, b]
```

#### Nested patterns.
The pattern depth is unlimited.
```yaml
pattern:
  type: stripes
  pattern1:
    color: [r, g, b]
  pattern2:
    type: checkers
    pattern1:
      color: [r, g, b]
    pattern2:
      color: [r, g, b]
```

