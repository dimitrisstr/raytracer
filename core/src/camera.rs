use crate::canvas::Canvas;
use crate::ray::Ray;
use crate::scene::Scene;
use math::{Matrix4f, Point4f, Vector4f};

/// A camera, that can be oriented and render a scene. The default position of the camera is the
/// point (0, 0, 0) and the default direction is the vector (0, 0, -1).
#[derive(Debug, PartialEq)]
pub struct Camera {
    /// Width (in pixels) of the canvas that the picture will be rendered to.
    width: usize,
    /// Height (in pixels) of the canvas that the picture will be rendered to.
    height: usize,
    /// Field of view, the angle of the camera.
    field_of_view: f64,
    /// The size of a pixel.
    pixel_size: f64,
    /// The transformation matrix that orients the scene.
    inv_view_transform: Matrix4f,
    half_width: f64,
    half_height: f64,
}

impl Camera {
    /// Creates a new camera.
    pub fn new(width: usize, height: usize, field_of_view: f64) -> Self {
        let mut camera = Self {
            width,
            height,
            field_of_view,
            pixel_size: 0.0,
            half_width: 0.0,
            half_height: 0.0,
            inv_view_transform: Matrix4f::new(),
        };
        camera.transform_view(
            Point4f::new(0., 0., 0.),
            Point4f::new(0., 0., -1.),
            Vector4f::new(0., 1., 0.),
        );
        camera.calculate_pixel_size();

        camera
    }

    /// Computes the transformation matrix, that orients the camera.
    #[rustfmt::skip]
    pub fn transform_view(&mut self, from: Point4f, to: Point4f, up: Vector4f) {
        let forward = (&to - &from).normalized();
        let left = forward.cross(&up.normalized());
        let true_up = left.cross(&forward);

        let orientation = Matrix4f::from([
                [    left.x,     left.y,     left.z, 0.],
                [ true_up.x,  true_up.y,  true_up.z, 0.],
                [-forward.x, -forward.y, -forward.z, 0.],
                [        0.,         0.,         0., 1.]
        ]);

        self.inv_view_transform =
            (&orientation * &Matrix4f::identity().translate(-from.x, -from.y, -from.z)).inv();
    }

    /// Renders a scene.
    pub fn render(&self, scene: &Scene) -> Canvas {
        let mut canvas = Canvas::new(self.width, self.height);
        for x in 0..self.width {
            for y in 0..self.height {
                let ray = self.generate_ray(x, y);
                let color = scene.trace(&ray, 3);
                canvas.set_pixel(x, y, color);
            }
        }
        canvas
    }

    /// Generates a ray that passes through the pixel (x, y).
    fn generate_ray(&self, x: usize, y: usize) -> Ray {
        // The offset from the edge of the canvas to the pixel's center.
        let x_offset = (x as f64 + 0.5) * self.pixel_size;
        let y_offset = (y as f64 + 0.5) * self.pixel_size;

        // The untransformed coordinates. The camera looks toward -z, so +x is to the left.
        let world_x = self.half_width - x_offset;
        let world_y = self.half_height - y_offset;

        // Transform the coordinates using the view transformation matrix. The canvas is at z = -1.
        let pixel = &self.inv_view_transform * &Point4f::new(world_x, world_y, -1.);
        let origin = &self.inv_view_transform * &Point4f::new(0., 0., 0.);
        let mut direction = &pixel - &origin;
        direction.normalize();

        Ray::new(origin, direction)
    }

    /// Calculates the size of the pixels.
    fn calculate_pixel_size(&mut self) {
        let half_view = (self.field_of_view / 2.).tan();
        let aspect = self.width as f64 / self.height as f64;

        if aspect >= 1. {
            self.half_width = half_view;
            self.half_height = half_view / aspect;
        } else {
            self.half_width = half_view * aspect;
            self.half_height = half_view;
        }

        self.pixel_size = (self.half_width * 2.) / self.width as f64;
    }
}

#[cfg(test)]
mod tests {
    use crate::camera::Camera;
    use crate::properties::Color;
    use crate::scene::Scene;
    use math::{utils, Matrix4f, Point4f, Vector4f};
    use std::f64::consts::{FRAC_1_SQRT_2, FRAC_PI_2, FRAC_PI_4};

    /// The default view transformation matrix of the camera.
    #[test]
    fn default_orientation() {
        let camera = Camera::new(100, 100, 0.5);

        assert_eq!(camera.inv_view_transform.inv(), Matrix4f::identity());
    }

    /// Ray generation.
    #[test]
    fn generate_ray() {
        let camera = Camera::new(201, 101, FRAC_PI_2);

        let ray = camera.generate_ray(100, 50);
        assert_eq!(ray.get_origin(), &Point4f::new(0., 0., 0.));
        assert_eq!(ray.get_direction(), &Vector4f::new(0., 0., -1.));

        let ray = camera.generate_ray(0, 0);
        assert_eq!(ray.get_origin(), &Point4f::new(0., 0., 0.));
        assert_eq!(
            ray.get_direction(),
            &Vector4f::new(0.66519, 0.33259, -0.66851)
        );
    }

    /// Ray generation with a custom view transformation matrix.
    #[test]
    fn generate_ray_transform() {
        let mut camera = Camera::new(201, 101, FRAC_PI_2);
        camera.inv_view_transform = Matrix4f::identity()
            .translate(0., -2., 5.)
            .rotate_y(FRAC_PI_4)
            .inv();
        let ray = camera.generate_ray(100, 50);

        assert_eq!(ray.get_origin(), &Point4f::new(0., 2., -5.));
        assert_eq!(
            ray.get_direction(),
            &Vector4f::new(FRAC_1_SQRT_2, 0., -FRAC_1_SQRT_2)
        );
    }

    /// View transformation matrix computation.
    #[test]
    #[rustfmt::skip]
    fn transform_view() {
        let mut camera = Camera::new(10, 20, 2.);
        camera.transform_view(
            Point4f::new(1., 3., 2.),
            Point4f::new(4., -2., 8.),
            Vector4f::new(1., 1., 0.),
        );

        assert_eq!(
            camera.inv_view_transform.inv(),
            Matrix4f::from([
                [-0.50709, 0.50709,  0.67612, -2.36643],
                [ 0.76772, 0.60609,  0.12122, -2.82843],
                [-0.35857, 0.59761, -0.71714,       0.],
                [      0.,      0.,       0.,       1.]
            ])
        );
    }

    /// Pixel size computation.
    #[test]
    fn pixel_size() {
        let expected_pixel_size = 0.01;
        let camera = Camera::new(200, 125, FRAC_PI_2);
        assert!(utils::eq(camera.pixel_size, expected_pixel_size));

        let camera = Camera::new(125, 200, FRAC_PI_2);
        assert!(utils::eq(camera.pixel_size, expected_pixel_size));
    }

    /// Scene rendering.
    #[test]
    fn render() {
        let scene = Scene::default();
        let mut camera = Camera::new(11, 11, FRAC_PI_2);
        camera.transform_view(
            Point4f::new(0., 0., -5.),
            Point4f::new(0., 0., 0.),
            Vector4f::new(0., 1., 0.),
        );
        let canvas = camera.render(&scene);

        assert_eq!(
            canvas.get_pixel(5, 5),
            Color {
                r: 0.38066,
                g: 0.47583,
                b: 0.2855
            }
        );
    }
}
