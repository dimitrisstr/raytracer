use crate::properties::Color;
use std::io::{BufWriter, Error, Write};

pub struct Canvas {
    width: usize,
    height: usize,
    pixels: Vec<Vec<Color>>,
}

impl Canvas {
    /// Creates a new width x height canvas.
    pub fn new(width: usize, height: usize) -> Self {
        let pixels = vec![
            vec![
                Color {
                    r: 0.,
                    g: 0.,
                    b: 0.
                };
                width
            ];
            height
        ];

        Self {
            width,
            height,
            pixels,
        }
    }

    /// Sets the color of a pixel. The x index must be in range [0, width) and the y index must be
    /// in range [0, height).
    ///
    /// # Panics
    ///
    /// Panics if x or y is out of bounds.
    pub fn set_pixel(&mut self, x: usize, y: usize, color: Color) {
        self.pixels[y][x] = color;
    }

    /// Returns the color of a pixel. The x index must in range [0, width) and the y index must be
    /// in range [0, height).
    ///
    /// # Panics
    ///
    /// Panics if x or y is out of bounds.
    pub fn get_pixel(&self, x: usize, y: usize) -> Color {
        self.pixels[y][x]
    }

    /// Creates a ppm image.
    pub fn to_ppm<T: Write>(&self, output: T) -> Result<T, Error> {
        let mut stream = BufWriter::new(output);
        // Writes ppm header.
        writeln!(stream, "P3")?;
        writeln!(stream, "{} {}", self.width, self.height)?;
        writeln!(stream, "255")?;

        // Writes the RGB values of the pixels.
        let mut line_length = 0;
        for row in &self.pixels {
            for pixel in row {
                let pixel = pixel.to_string() + " ";
                line_length += pixel.len();

                // Max line length is 70 characters.
                if line_length > 70 {
                    stream.write_all(b"\n")?;
                    line_length = pixel.len();
                }

                stream.write_all(pixel.as_bytes())?;
            }
        }

        stream.flush()?;
        Ok(stream.into_inner()?)
    }

    pub fn width(&self) -> usize {
        self.width
    }

    pub fn height(&self) -> usize {
        self.height
    }
}

#[cfg(test)]
mod tests {
    use crate::canvas::Canvas;
    use crate::properties::Color;

    #[test]
    fn new() {
        let canvas = Canvas::new(10, 20);

        for x in 0..10 {
            for y in 0..20 {
                assert_eq!(
                    canvas.get_pixel(x, y),
                    Color {
                        r: 0.,
                        g: 0.,
                        b: 0.
                    }
                );
            }
        }
    }

    #[test]
    fn set_pixel() {
        let mut canvas = Canvas::new(10, 10);
        canvas.set_pixel(
            5,
            7,
            Color {
                r: 0.3,
                g: 0.5,
                b: 0.9,
            },
        );

        assert_eq!(
            &canvas.pixels[7][5],
            &Color {
                r: 0.3,
                g: 0.5,
                b: 0.9
            }
        );
    }

    #[test]
    fn get_pixel() {
        let mut canvas = Canvas::new(10, 10);
        canvas.pixels[7][5] = Color {
            r: 0.3,
            g: 0.5,
            b: 0.9,
        };

        assert_eq!(
            canvas.get_pixel(5, 7),
            Color {
                r: 0.3,
                g: 0.5,
                b: 0.9
            }
        );
    }

    #[test]
    fn to_ppm() {
        let mut canvas = Canvas::new(10, 2);
        for y in 0..2 {
            for x in 0..10 {
                canvas.set_pixel(
                    x,
                    y,
                    Color {
                        r: 1.,
                        g: 0.8,
                        b: 0.6,
                    },
                );
            }
        }

        assert_eq!(
            String::from_utf8(canvas.to_ppm(Vec::new()).unwrap()).unwrap(),
            "P3\n10 2\n255\n255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 "
        );
    }
}
