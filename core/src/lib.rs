pub mod camera;
pub mod canvas;
pub mod properties;
pub mod scene;
pub mod scene_parser;

mod light;
mod pattern;
mod ray;
mod shapes;
