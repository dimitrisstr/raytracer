use crate::properties::Color;
use math::{Matrix4f, Point4f};

pub trait Pattern {
    fn color_at(&self, point: &Point4f) -> Color;
}

/// Macro that creates a pattern with two nested patterns.
macro_rules! create_pattern {
    ($pattern: ident) => {
        pub struct $pattern {
            pattern1: Box<dyn Pattern>,
            pattern2: Box<dyn Pattern>,
            inv_transform: Matrix4f,
        }

        impl $pattern {
            pub fn new(pattern1: Box<dyn Pattern>, pattern2: Box<dyn Pattern>) -> Self {
                Self {
                    pattern1,
                    pattern2,
                    inv_transform: Matrix4f::identity(),
                }
            }

            pub fn with_transform(
                pattern1: Box<dyn Pattern>,
                pattern2: Box<dyn Pattern>,
                transform: Matrix4f,
            ) -> Self {
                Self {
                    pattern1,
                    pattern2,
                    inv_transform: transform.inv(),
                }
            }
        }
    };
}

/// A stripes pattern.
impl Pattern for Stripes {
    fn color_at(&self, point: &Point4f) -> Color {
        let pattern_point = &self.inv_transform * point;
        if pattern_point.x.floor() as i32 % 2 == 0 {
            self.pattern1.color_at(&pattern_point)
        } else {
            self.pattern2.color_at(&pattern_point)
        }
    }
}

create_pattern!(Stripes);

/// A gradient pattern.
impl Pattern for Gradient {
    fn color_at(&self, point: &Point4f) -> Color {
        let pattern_point = &self.inv_transform * point;
        let color1 = self.pattern1.color_at(&pattern_point);
        let color2 = self.pattern2.color_at(&pattern_point);
        let distance = color2 - color1;
        let fraction = pattern_point.x - pattern_point.x.floor();

        color1 + distance * fraction
    }
}

create_pattern!(Gradient);

/// A ring pattern.
impl Pattern for Ring {
    fn color_at(&self, point: &Point4f) -> Color {
        let pattern_point = &self.inv_transform * point;
        let a = (pattern_point.x.powi(2) + pattern_point.z.powi(2))
            .sqrt()
            .floor() as i32;
        if a % 2 == 0 {
            self.pattern1.color_at(&pattern_point)
        } else {
            self.pattern2.color_at(&pattern_point)
        }
    }
}

create_pattern!(Ring);

/// A checkers pattern.
impl Pattern for Checkers {
    fn color_at(&self, point: &Point4f) -> Color {
        let pattern_point = &self.inv_transform * point;
        let px = pattern_point.x.floor() as i32;
        let py = pattern_point.y.floor() as i32;
        let pz = pattern_point.z.floor() as i32;

        if (px + py + pz) % 2 == 0 {
            self.pattern1.color_at(&pattern_point)
        } else {
            self.pattern2.color_at(&pattern_point)
        }
    }
}

create_pattern!(Checkers);

/// The leaf of a pattern is a color.
impl Pattern for Color {
    fn color_at(&self, _point: &Point4f) -> Color {
        *self
    }
}

#[cfg(test)]
mod tests {
    mod stripes {
        use crate::pattern::{Pattern, Stripes};
        use crate::properties::{Material, BLACK, WHITE};
        use crate::shapes::{Shape, ShapeProperties, Sphere};
        use math::{Matrix4f, Point4f};

        /// A stripe pattern is constant in y.
        #[test]
        fn color_at_point_y() {
            let pattern = Stripes::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 1., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 2., 0.)), WHITE);
        }

        /// A stripe pattern is constant in z.
        #[test]
        fn color_at_point_z() {
            let pattern = Stripes::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 1.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 2.)), WHITE);
        }

        /// A stripe pattern alternates in x.
        #[test]
        fn color_at_point_x() {
            let pattern = Stripes::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0.9, 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(1., 0., 0.)), BLACK);
            assert_eq!(pattern.color_at(&Point4f::new(-0.1, 0., 0.)), BLACK);
            assert_eq!(pattern.color_at(&Point4f::new(-1., 0., 0.)), BLACK);
            assert_eq!(pattern.color_at(&Point4f::new(-1.1, 0., 0.)), WHITE);
        }

        /// Stripes with object transformation.
        #[test]
        fn color_at_with_object_transform() {
            let pattern = Stripes::new(Box::new(WHITE), Box::new(BLACK));
            let sphere = Sphere::new(ShapeProperties::with_transform(
                Box::new(pattern),
                Material::default(),
                Matrix4f::identity().scale(2., 2., 2.),
            ));

            assert_eq!(sphere.color_at(&Point4f::new(1.5, 0., 0.)), WHITE);
        }

        /// Stripes with pattern transformation.
        #[test]
        fn color_at_with_pattern_transform() {
            let pattern = Stripes::with_transform(
                Box::new(WHITE),
                Box::new(BLACK),
                Matrix4f::identity().scale(2., 2., 2.),
            );
            let sphere = Sphere::new(ShapeProperties::new(Box::new(pattern), Material::default()));

            assert_eq!(sphere.color_at(&Point4f::new(1.5, 0., 0.)), WHITE);
        }

        /// Stripes with both object and pattern transformation.
        #[test]
        fn color_at_with_object_pattern_transform() {
            let pattern = Stripes::with_transform(
                Box::new(WHITE),
                Box::new(BLACK),
                Matrix4f::identity().translate(0.5, 0., 0.),
            );
            let sphere = Sphere::new(ShapeProperties::with_transform(
                Box::new(pattern),
                Material::default(),
                Matrix4f::identity().scale(2., 2., 2.),
            ));

            assert_eq!(sphere.color_at(&Point4f::new(2.4, 0., 0.)), WHITE);
        }
    }

    mod gradient {
        use crate::pattern::{Gradient, Pattern};
        use crate::properties::{Color, BLACK, WHITE};
        use math::Point4f;

        #[test]
        fn color_at() {
            let pattern = Gradient::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(
                pattern.color_at(&Point4f::new(0.25, 0., 0.)),
                Color {
                    r: 0.75,
                    g: 0.75,
                    b: 0.75
                }
            );
            assert_eq!(
                pattern.color_at(&Point4f::new(0.5, 0., 0.)),
                Color {
                    r: 0.5,
                    g: 0.5,
                    b: 0.5
                }
            );
            assert_eq!(
                pattern.color_at(&Point4f::new(0.75, 0., 0.)),
                Color {
                    r: 0.25,
                    g: 0.25,
                    b: 0.25
                }
            );
        }
    }

    mod ring {
        use crate::pattern::{Pattern, Ring};
        use crate::properties::{BLACK, WHITE};
        use math::Point4f;

        #[test]
        fn color_at() {
            let pattern = Ring::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(1., 0., 0.)), BLACK);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 1.)), BLACK);
            assert_eq!(pattern.color_at(&Point4f::new(0.708, 0., 0.708)), BLACK);
        }
    }

    mod checkers {
        use crate::pattern::{Checkers, Pattern};
        use crate::properties::{BLACK, WHITE};
        use math::Point4f;

        /// Checkers should repeat in x.
        #[test]
        fn color_at_x() {
            let pattern = Checkers::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0.99, 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(1.01, 0., 0.)), BLACK);
        }

        /// Checkers should repeat in y.
        #[test]
        fn color_at_y() {
            let pattern = Checkers::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0.99, 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 1.01, 0.)), BLACK);
        }

        /// Checkers should repeat in z.
        #[test]
        fn color_at_z() {
            let pattern = Checkers::new(Box::new(WHITE), Box::new(BLACK));

            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 0.99)), WHITE);
            assert_eq!(pattern.color_at(&Point4f::new(0., 0., 1.01)), BLACK);
        }
    }
}
