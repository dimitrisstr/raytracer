use math::{Matrix4f, Point4f, Vector4f};

/// Represents a ray in 3D space.
pub struct Ray {
    /// The origin of the ray.
    origin: Point4f,
    /// The direction of the ray.
    direction: Vector4f,
}

impl Ray {
    /// Creates a new ray.
    pub fn new(origin: Point4f, direction: Vector4f) -> Self {
        Self { origin, direction }
    }

    /// Returns the point at distance t.
    pub fn position(&self, t: f64) -> Point4f {
        &self.origin + &(&self.direction * t)
    }

    /// Creates a transformed ray.
    pub fn transform(&self, transformation: &Matrix4f) -> Self {
        let origin = transformation * &self.origin;
        let direction = transformation * &self.direction;

        Ray::new(origin, direction)
    }

    /// Returns the origin of the ray.
    pub fn get_origin(&self) -> &Point4f {
        &self.origin
    }

    /// Returns the direction of the ray.
    pub fn get_direction(&self) -> &Vector4f {
        &self.direction
    }
}

#[cfg(test)]
mod tests {
    use crate::ray::Ray;
    use math::{Matrix4f, Point4f, Vector4f};

    #[test]
    fn position() {
        let ray = Ray::new(Point4f::new(2., 3., 4.), Vector4f::new(1., 0., 0.));

        assert_eq!(ray.position(0.), Point4f::new(2., 3., 4.));
        assert_eq!(ray.position(1.), Point4f::new(3., 3., 4.));
        assert_eq!(ray.position(-1.), Point4f::new(1., 3., 4.));
        assert_eq!(ray.position(2.5), Point4f::new(4.5, 3., 4.));
    }

    #[test]
    fn transform() {
        let ray = Ray::new(Point4f::new(1., 2., 3.), Vector4f::new(0., 1., 0.));
        let transformed_ray = ray.transform(&Matrix4f::identity().translate(3., 4., 5.));

        assert_eq!(transformed_ray.origin, Point4f::new(4., 6., 8.));
        assert_eq!(transformed_ray.direction, Vector4f::new(0., 1., 0.));

        let transformed_ray = ray.transform(&Matrix4f::identity().scale(2., 3., 4.));

        assert_eq!(transformed_ray.origin, Point4f::new(2., 6., 12.));
        assert_eq!(transformed_ray.direction, Vector4f::new(0., 3., 0.))
    }
}
