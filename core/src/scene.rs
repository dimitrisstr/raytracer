use crate::light::{Light, PointLight};
use crate::properties::{Color, Material, BLACK, WHITE};
use crate::ray::Ray;
use crate::shapes::{Intersection, IntersectionRecorder, Shape, ShapeProperties, Sphere};
use math::{utils, Matrix4f, Point4f, Vector4f};

/// A scene is a group of shapes and lights.
pub struct Scene {
    shapes: Vec<Box<dyn Shape>>,
    lights: Vec<Box<dyn Light>>,
}

impl Scene {
    /// Creates a new empty scene.
    pub fn new() -> Self {
        Self {
            shapes: Vec::new(),
            lights: Vec::new(),
        }
    }

    /// Adds a shape to the scene.
    pub fn add_shape(&mut self, shape: Box<dyn Shape>) {
        self.shapes.push(shape);
    }

    /// Adds a light to the scene.
    pub fn add_light(&mut self, light: Box<dyn Light>) {
        self.lights.push(light);
    }

    /// Returns the color of the ray.
    pub fn trace(&self, ray: &Ray, depth: usize) -> Color {
        let intersection_recorder = self.intersect(ray, false);
        if intersection_recorder.hit().is_some() {
            self.shade(ray, intersection_recorder, depth)
        } else {
            BLACK
        }
    }

    /// Intersects the ray with all the shapes and records all the intersections. If
    /// first_hit is true, the method intersects the shapes until it finds a positive
    /// intersection.
    fn intersect(&self, ray: &Ray, first_hit: bool) -> IntersectionRecorder {
        let mut intersection_recorder = IntersectionRecorder::new();
        for obj in &self.shapes {
            obj.intersect(ray, &mut intersection_recorder);
            if first_hit && intersection_recorder.hit().is_some() {
                break;
            }
        }
        intersection_recorder
    }

    /// Computes the color of an intersection.
    fn shade(&self, ray: &Ray, intersection_rec: IntersectionRecorder, depth: usize) -> Color {
        match Computations::compute(ray, intersection_rec) {
            Some(comps) => {
                let surface_color = self.surface_color(
                    &comps.over_point,
                    &comps.eye_v,
                    &comps.normal,
                    comps.hit_shape,
                );
                let reflected_color = self.reflected_color(
                    &comps.over_point,
                    ray.get_direction(),
                    &comps.normal,
                    comps.hit_shape.get_material().reflective,
                    depth,
                );
                let refracted_color = self.refracted_color(
                    &comps.under_point,
                    &comps.eye_v,
                    &comps.normal,
                    (comps.n1, comps.n2),
                    comps.hit_shape.get_material().transparency,
                    depth,
                );

                if let Some(reflectance) = comps.reflectance {
                    return surface_color
                        + reflected_color * reflectance
                        + refracted_color * (1. - reflectance);
                }

                surface_color + reflected_color + refracted_color
            }
            _ => BLACK,
        }
    }

    /// Computes the color of the surface.
    fn surface_color(
        &self,
        hit_point: &Point4f,
        eye_v: &Vector4f,
        normal: &Vector4f,
        shape: &dyn Shape,
    ) -> Color {
        let mut color = BLACK;
        for light in self.lights.iter() {
            color += light.lighting(
                hit_point,
                eye_v,
                normal,
                shape.get_material(),
                shape.color_at(hit_point),
                self.is_shadowed(light.get_position(), hit_point),
            );
        }
        color
    }

    /// Computes the reflected color.
    fn reflected_color(
        &self,
        hit_point: &Point4f,
        ray_v: &Vector4f,
        normal: &Vector4f,
        reflective: f64,
        depth: usize,
    ) -> Color {
        if reflective <= 0. || depth == 0 {
            return BLACK;
        }

        let reflect_v = ray_v.reflect(normal);
        let reflected_ray = Ray::new(hit_point.clone(), reflect_v);
        self.trace(&reflected_ray, depth - 1) * reflective
    }

    /// Computes the refracted color.
    fn refracted_color(
        &self,
        hit_point: &Point4f,
        eye_v: &Vector4f,
        normal: &Vector4f,
        refractive_indexes: (f64, f64),
        transparency: f64,
        depth: usize,
    ) -> Color {
        if transparency <= 0. || depth == 0 {
            return BLACK;
        }

        let n_ratio = refractive_indexes.0 / refractive_indexes.1;
        let cos_i = eye_v.dot(normal);
        let sin2_t = n_ratio.powi(2) * (1. - cos_i.powi(2));
        if sin2_t > 1. {
            return BLACK;
        }

        let cos_t = (1. - sin2_t).sqrt();
        let direction = &(normal * (n_ratio * cos_i - cos_t)) - &(eye_v * n_ratio);
        let refracted_ray = Ray::new(hit_point.clone(), direction);
        return self.trace(&refracted_ray, depth) * transparency;
    }

    /// Checks if a point is shadowed. Casts a ray from the hit point to the light position and
    /// checks for hits.
    fn is_shadowed(&self, light_position: &Point4f, hit_point: &Point4f) -> bool {
        let light_v = light_position - hit_point;
        let distance = light_v.length();
        let direction = light_v.normalized();

        let ray = Ray::new(hit_point.clone(), direction);
        let intersection_recorder = self.intersect(&ray, true);
        intersection_recorder.hit().is_some() && intersection_recorder.hit().unwrap().t < distance
    }
}

impl Default for Scene {
    fn default() -> Self {
        let mut scene = Scene::new();
        let point_light = PointLight::new(Point4f::new(-10., 10., -10.));
        let sphere1 = Sphere::new(ShapeProperties::new(
            Box::new(Color::new(0.8, 1., 0.6)),
            Material {
                specular: 0.2,
                diffuse: 0.7,
                ..Material::default()
            },
        ));

        let sphere2 = Sphere::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material::default(),
            Matrix4f::identity().scale(0.5, 0.5, 0.5),
        ));

        scene.add_shape(Box::new(sphere1));
        scene.add_shape(Box::new(sphere2));
        scene.add_light(Box::new(point_light));
        scene
    }
}

/// A struct that is used as a cache for all the computations that are needed
/// in ray tracing, reflection and refraction.
struct Computations<'a> {
    /// The refractive index of the object the ray is exciting.
    n1: f64,
    /// The refractive index of the object the ray is entering.
    n2: f64,
    /// A vector from the intersection to the camera.
    eye_v: Vector4f,
    /// A vector that is perpendicular to the intersection.
    normal: Vector4f,
    /// A point that is slightly below the intersection point.
    under_point: Point4f,
    /// A point that is slightly above the intersection point.
    over_point: Point4f,
    /// The shape of the lowest positive intersection.
    hit_shape: &'a dyn Shape,
    /// The fraction of the light that is reflected.
    reflectance: Option<f64>,
}

impl<'a> Computations<'a> {
    /// Computes all the variables.
    fn compute(ray: &Ray, intersections_rec: IntersectionRecorder<'a>) -> Option<Computations<'a>> {
        match intersections_rec.hit() {
            Some(Intersection {
                t: hit_t,
                shape: hit_shape,
            }) => {
                let (n1, n2) = if hit_shape.get_material().transparency > 0. {
                    Computations::refractive_indexes(&intersections_rec, hit_t)
                } else {
                    (1., 1.)
                };

                let eye_v = -ray.get_direction();
                let hit_point = ray.position(hit_t);
                let mut normal = hit_shape.normal_at(&hit_point);
                if Computations::is_inside_hit(&normal, &eye_v) {
                    normal = -&normal;
                }

                let over_point = Computations::over_point(&hit_point, &normal);
                let under_point = Computations::under_point(&hit_point, &normal);

                let reflectance = if hit_shape.get_material().reflective > 0.
                    && hit_shape.get_material().transparency > 0.
                {
                    Some(Computations::schlick(n1, n2, &eye_v, &normal))
                } else {
                    None
                };

                Some(Computations {
                    n1,
                    n2,
                    eye_v,
                    normal,
                    under_point,
                    over_point,
                    hit_shape,
                    reflectance,
                })
            }
            _ => None,
        }
    }

    /// Checks if the ray intersects a shape from the inside.
    fn is_inside_hit(normal: &Vector4f, eye_v: &Vector4f) -> bool {
        normal.dot(eye_v) < 0.
    }

    /// Returns a new point that is slightly above the old point. This is used to avoid acne in
    /// images.
    fn over_point(point: &Point4f, normal: &Vector4f) -> Point4f {
        point + &(normal * utils::EPSILON)
    }

    /// Returns a new point that is slightly below the old point.
    fn under_point(point: &Point4f, normal: &Vector4f) -> Point4f {
        point - &(normal * utils::EPSILON)
    }

    /// Computes the schlick approximation of the Fresnel effect.
    fn schlick(n1: f64, n2: f64, eye_v: &Vector4f, normal: &Vector4f) -> f64 {
        let mut cos = eye_v.dot(normal);
        if n1 > n2 {
            let n = n1 / n2;
            let sin2_t = n.powi(2) * (1. - cos.powi(2));
            if sin2_t > 1. {
                return 1.;
            }

            cos = (1. - sin2_t).sqrt();
        }
        let r0 = ((n1 - n2) / (n1 + n2)).powi(2);
        r0 + (1. - r0) * (1. - cos).powi(5)
    }

    /// Computes the refractive indexes n1, n2 for intersection hit_t.
    /// * n1 is the refractive index of the material being exited
    /// * n2 is the refractive index of the material being entered
    fn refractive_indexes(intersections_rec: &IntersectionRecorder, hit_t: f64) -> (f64, f64) {
        let mut containers: Vec<&Intersection> = Vec::new();
        let intersections = intersections_rec.sorted();

        let (mut n1, mut n2) = (1., 1.);
        for intersection in intersections.iter() {
            if intersection.t == hit_t {
                n1 = match containers.last() {
                    Some(Intersection { shape, .. }) => shape.get_material().refractive_index,
                    None => 1.,
                };
            }

            if let Some(index) = containers.iter().position(|item| {
                std::ptr::eq(item.shape.get_material(), intersection.shape.get_material())
            }) {
                containers.remove(index);
            } else {
                containers.push(intersection);
            };

            if intersection.t == hit_t {
                n2 = match containers.last() {
                    Some(Intersection { shape, .. }) => shape.get_material().refractive_index,
                    None => 1.,
                };
                break;
            }
        }

        (n1, n2)
    }
}

#[cfg(test)]
mod tests {
    use crate::light::PointLight;
    use crate::properties::{Color, Material, BLACK, WHITE};
    use crate::ray::Ray;
    use crate::scene::{Computations, Scene};
    use crate::shapes::{IntersectionRecorder, Plane, Shape, ShapeProperties, Sphere};
    use math::{utils, Matrix4f, Point4f, Vector4f};
    use std::f64::consts::{FRAC_1_SQRT_2, SQRT_2};
    use std::ops::Deref;

    fn create_reflection_test_fixture() -> (Scene, Ray) {
        let shape = Plane::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                reflective: 0.5,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -1., 0.),
        ));

        let mut scene = Scene::default();
        scene.add_shape(Box::new(shape));

        let ray = Ray::new(
            Point4f::new(0., 0., -3.),
            Vector4f::new(0., -FRAC_1_SQRT_2, FRAC_1_SQRT_2),
        );

        (scene, ray)
    }

    fn create_refraction_test_fixture() -> Scene {
        let point_light = PointLight::new(Point4f::new(-10., 10., -10.));
        let sphere1 = Sphere::new(ShapeProperties::new(
            Box::new(Color::new(0.8, 1., 0.6)),
            Material {
                specular: 0.2,
                diffuse: 0.7,
                transparency: 1.,
                refractive_index: 1.5,
                ..Material::default()
            },
        ));
        let sphere2 = Sphere::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                transparency: 1.,
                refractive_index: 1.5,
                ..Material::default()
            },
            Matrix4f::identity().scale(0.5, 0.5, 0.5),
        ));

        let mut scene = Scene::new();
        scene.add_shape(Box::new(sphere1));
        scene.add_shape(Box::new(sphere2));
        scene.add_light(Box::new(point_light));
        scene
    }

    fn create_glass_sphere(refractive_index: f64, transform: Matrix4f) -> Sphere {
        Sphere::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                refractive_index,
                transparency: 1.,
                ..Material::default()
            },
            transform,
        ))
    }

    /// The color when a ray misses.
    #[test]
    fn trace_miss() {
        let scene = Scene::default();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 1., 0.));

        assert_eq!(scene.trace(&ray, 0), BLACK);
    }

    /// The color when a ray hits.
    #[test]
    fn trace_hit() {
        let scene = Scene::default();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));

        assert_eq!(
            scene.trace(&ray, 0),
            Color {
                r: 0.38066,
                g: 0.47583,
                b: 0.2855
            }
        );
    }

    #[test]
    fn trace() {
        let scene = Scene::default();
        let ray = Ray::new(Point4f::new(0., 0., 0.75), Vector4f::new(0., 0., -1.));

        assert_eq!(
            scene.trace(&ray, 0),
            Color {
                r: 0.1,
                g: 0.1,
                b: 0.1
            }
        );
    }

    /// A ray intersects the scene.
    #[test]
    fn intersect() {
        let scene = Scene::default();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let intersections = scene
            .intersect(&ray, false)
            .sorted()
            .iter()
            .map(|inter| inter.t)
            .collect::<Vec<f64>>();

        assert_eq!(intersections, vec![4., 4.5, 5.5, 6.]);
    }

    /// Shade an outside intersection.
    #[test]
    fn shade_outside() {
        let scene = Scene::default();
        let sphere1 = scene.shapes.get(0).unwrap();
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));

        let mut intersection_recorder = IntersectionRecorder::new();
        sphere1.intersect(&ray, &mut intersection_recorder);

        assert_eq!(
            scene.shade(&ray, intersection_recorder, 0),
            Color {
                r: 0.38066,
                g: 0.47583,
                b: 0.2855
            }
        )
    }

    /// Shade an inside intersection.
    #[test]
    fn shade_inside() {
        let mut scene = Scene::default();
        scene.lights.clear();
        scene.add_light(Box::new(PointLight::new(Point4f::new(0., 0.25, 0.))));
        let sphere2 = scene.shapes.get(1).unwrap();
        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 0., 1.));

        let mut intersection_recorder = IntersectionRecorder::new();
        sphere2.intersect(&ray, &mut intersection_recorder);

        assert_eq!(
            scene.shade(&ray, intersection_recorder, 0),
            Color {
                r: 0.90498,
                g: 0.90498,
                b: 0.90498
            }
        );
    }

    #[test]
    fn shade_reflective_material() {
        let (scene, ray) = create_reflection_test_fixture();
        let shape = scene.shapes.get(2).unwrap().deref();
        let mut intersection_rec = IntersectionRecorder::new();
        intersection_rec.add(shape, SQRT_2);

        assert_eq!(
            scene.shade(&ray, intersection_rec, 1),
            Color {
                r: 0.87675,
                g: 0.92434,
                b: 0.82918
            }
        );
    }

    #[test]
    fn shade_transparent_material() {
        let floor = Plane::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                transparency: 0.5,
                refractive_index: 1.5,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -1., 0.),
        ));
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(Color::new(1., 0., 0.)),
            Material {
                ambient: 0.5,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -3.5, -0.5),
        ));

        let mut scene = Scene::default();
        scene.add_shape(Box::new(floor));
        scene.add_shape(Box::new(sphere));
        let ray = Ray::new(
            Point4f::new(0., 0., -3.),
            Vector4f::new(0., -FRAC_1_SQRT_2, FRAC_1_SQRT_2),
        );

        let color = scene.trace(&ray, 5);
        assert_eq!(color, Color::new(0.93642, 0.68642, 0.68642));
    }

    #[test]
    fn shade_reflective_transparent_material() {
        let floor = Plane::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                reflective: 0.5,
                transparency: 0.5,
                refractive_index: 1.5,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -1., 0.),
        ));
        let sphere = Sphere::new(ShapeProperties::with_transform(
            Box::new(Color::new(1., 0., 0.)),
            Material {
                ambient: 0.5,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -3.5, -0.5),
        ));

        let mut scene = Scene::default();
        scene.add_shape(Box::new(floor));
        scene.add_shape(Box::new(sphere));
        let ray = Ray::new(
            Point4f::new(0., 0., -3.),
            Vector4f::new(0., -FRAC_1_SQRT_2, FRAC_1_SQRT_2),
        );

        let color = scene.trace(&ray, 5);
        assert_eq!(color, Color::new(0.93391, 0.69643, 0.69243));
    }

    #[test]
    fn reflected_color_non_reflective_material() {
        let scene = Scene::default();
        let shape = scene.shapes.get(1).unwrap().deref();
        let mut intersection_recorder = IntersectionRecorder::new();
        intersection_recorder.add(shape, 1.);
        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 0., 1.));
        let comps = Computations::compute(&ray, intersection_recorder).unwrap();

        let color = scene.reflected_color(
            &comps.over_point,
            ray.get_direction(),
            &comps.normal,
            comps.hit_shape.get_material().reflective,
            1,
        );
        assert_eq!(color, BLACK);
    }

    /// The reflected color of a reflective material.
    #[test]
    fn reflected_color_reflective_material() {
        let (scene, ray) = create_reflection_test_fixture();
        let mut intersection_recorder = IntersectionRecorder::new();
        let shape = scene.shapes.get(2).unwrap().deref();
        intersection_recorder.add(shape, SQRT_2);
        let comps = Computations::compute(&ray, intersection_recorder).unwrap();

        let expected_colors = vec![BLACK, Color::new(0.19033, 0.23791, 0.14274)];
        for depth in 0..=1 {
            let color = scene.reflected_color(
                &comps.over_point,
                ray.get_direction(),
                &comps.normal,
                comps.hit_shape.get_material().reflective,
                depth,
            );
            assert_eq!(color, expected_colors[depth]);
        }
    }

    /// Test for infinite recursion.
    #[test]
    fn reflected_color_recursion() {
        let light = PointLight::new(Point4f::new(0., 0., 0.));
        let lower_plane = Plane::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                reflective: 1.,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., -1., 0.),
        ));
        let upper_plane = Plane::new(ShapeProperties::with_transform(
            Box::new(WHITE),
            Material {
                reflective: 1.,
                ..Material::default()
            },
            Matrix4f::identity().translate(0., 1., 0.),
        ));

        let mut scene = Scene::default();
        scene.add_shape(Box::new(lower_plane));
        scene.add_shape(Box::new(upper_plane));
        scene.add_light(Box::new(light));

        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 1., 0.));
        scene.trace(&ray, 3);
    }

    #[test]
    fn refracted_color_opaque_material() {
        let scene = Scene::default();
        let shape = scene.shapes.get(0).unwrap().deref();
        let mut intersection_recorder = IntersectionRecorder::new();
        intersection_recorder.add(shape, 4.);
        intersection_recorder.add(shape, 6.);
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let comps = Computations::compute(&ray, intersection_recorder).unwrap();

        let color = scene.refracted_color(
            &comps.under_point,
            &comps.eye_v,
            &comps.normal,
            (comps.n1, comps.n2),
            comps.hit_shape.get_material().transparency,
            1,
        );
        assert_eq!(color, BLACK);
    }

    #[test]
    fn refracted_color_max_recursion_depth() {
        let scene = create_refraction_test_fixture();
        let shape = scene.shapes.get(0).unwrap().deref();
        let mut intersection_recorder = IntersectionRecorder::new();
        intersection_recorder.add(shape, 4.);
        intersection_recorder.add(shape, 6.);
        let ray = Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.));
        let comps = Computations::compute(&ray, intersection_recorder).unwrap();

        let color = scene.refracted_color(
            &comps.over_point,
            &comps.eye_v,
            &comps.normal,
            (comps.n1, comps.n2),
            comps.hit_shape.get_material().transparency,
            0,
        );
        assert_eq!(color, BLACK);
    }

    #[test]
    fn refracted_color_total_internal_reflection() {
        let scene = create_refraction_test_fixture();
        let shape = scene.shapes.get(0).unwrap().deref();
        let mut intersection_recorder = IntersectionRecorder::new();
        intersection_recorder.add(shape, -FRAC_1_SQRT_2);
        intersection_recorder.add(shape, FRAC_1_SQRT_2);
        let ray = Ray::new(
            Point4f::new(0., 0., FRAC_1_SQRT_2),
            Vector4f::new(0., 1., 0.),
        );
        let comps = Computations::compute(&ray, intersection_recorder).unwrap();

        let color = scene.refracted_color(
            &comps.over_point,
            &comps.eye_v,
            &comps.normal,
            (comps.n1, comps.n2),
            comps.hit_shape.get_material().transparency,
            5,
        );
        assert_eq!(color, BLACK);
    }

    #[test]
    fn is_shadowed() {
        let scene = Scene::default();
        let light = scene.lights.get(0).unwrap();

        assert!(!scene.is_shadowed(light.get_position(), &Point4f::new(0., 10., 0.)),);
        assert!(scene.is_shadowed(light.get_position(), &Point4f::new(10., -10., 10.)),);
        assert!(!scene.is_shadowed(light.get_position(), &Point4f::new(-20., 20., -20.)),);
        assert!(!scene.is_shadowed(light.get_position(), &Point4f::new(-2., 2., -2.)),);
    }

    #[test]
    fn over_point() {
        let point = Point4f::new(1., 2.2, -4.);
        let normal = Vector4f::new(-1., 0., 1.);

        assert_eq!(
            Computations::over_point(&point, &normal),
            Point4f::new(0.99999, 2.2, -3.99999)
        )
    }

    #[test]
    fn under_point() {
        let point = Point4f::new(1., 2.2, -4.);
        let normal = Vector4f::new(-1., 0., 1.);

        assert_eq!(
            Computations::under_point(&point, &normal),
            Point4f::new(1.00001, 2.2, -4.00001)
        )
    }

    #[test]
    fn schlick_total_internal_reflection() {
        let ray = Ray::new(
            Point4f::new(0., 0., FRAC_1_SQRT_2),
            Vector4f::new(0., 1., 0.),
        );
        let hit_point = ray.position(FRAC_1_SQRT_2);
        let eye_v = -ray.get_direction();
        let sphere = create_glass_sphere(1.5, Matrix4f::identity());
        let normal = sphere.normal_at(&hit_point);
        let reflectance = Computations::schlick(1.5, 1., &eye_v, &normal);

        assert!(utils::eq(reflectance, 1.));
    }

    #[test]
    fn schlick_perpendicular_ray() {
        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 1., 0.));
        let hit_point = ray.position(1.);
        let eye_v = -ray.get_direction();
        let sphere = create_glass_sphere(1.5, Matrix4f::identity());
        let normal = -&sphere.normal_at(&hit_point);
        let reflectance = Computations::schlick(1., 1.5, &eye_v, &normal);

        assert!(utils::eq(reflectance, 0.04));
    }

    #[test]
    fn schlick_n2_greater_than_n1() {
        let ray = Ray::new(Point4f::new(0., 0.99, -2.), Vector4f::new(0., 0., 1.));
        let hit_point = ray.position(1.8589);
        let eye_v = -ray.get_direction();
        let sphere = create_glass_sphere(1.5, Matrix4f::identity());
        let normal = sphere.normal_at(&hit_point);
        let reflectance = Computations::schlick(1., 1.5, &eye_v, &normal);

        assert!(utils::eq(reflectance, 0.48873));
    }

    /// The scene of this test has 3 transparent spheres A, B and C. The test checks the
    /// computation of n1 and n2 at the intersections.
    ///               
    ///                                  _    _
    ///                             -       A      -
    ///                         -                      -
    ///                      -      =   =    #   #        -
    ///                    -    =        #  =        #      -
    ///                   -   =        #      =       #      -
    ///                  -   =       #         =       #      -
    ///  ----------------|--|--------|---------|-------|------|-----------------------------------
    ///                  -   =       #         =       #      -
    ///                   -   =  B     #      =   C   #      -
    ///                    -    =        #  =        #      -
    ///                      -      =   =    #   #        -
    ///                         -                      -
    ///                             -              -
    ///                                  -    -
    #[test]
    fn refractive_indexes() {
        let a = create_glass_sphere(1.5, Matrix4f::identity().scale(2., 2., 2.));
        let b = create_glass_sphere(2., Matrix4f::identity().translate(0., 0., -0.25));
        let c = create_glass_sphere(2.5, Matrix4f::identity().translate(0., 0., 0.25));

        let mut scene = Scene::new();
        scene.add_shape(Box::new(a));
        scene.add_shape(Box::new(b));
        scene.add_shape(Box::new(c));

        let ray = Ray::new(Point4f::new(0., 0., -4.), Vector4f::new(0., 0., 1.));
        let intersections_rec = scene.intersect(&ray, false);
        let intersections = intersections_rec.sorted();

        assert_eq!(intersections_rec.len(), 6);
        for (index, refractive_indexes) in [
            (1., 1.5),
            (1.5, 2.),
            (2., 2.5),
            (2.5, 2.5),
            (2.5, 1.5),
            (1.5, 1.),
        ]
        .into_iter()
        .enumerate()
        {
            let hit_t = intersections.get(index).unwrap().t;
            assert_eq!(
                Computations::refractive_indexes(&intersections_rec, hit_t),
                refractive_indexes
            );
        }
    }
}
