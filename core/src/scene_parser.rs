use crate::camera::Camera;
use crate::light::{Light, PointLight};
use crate::pattern::{Checkers, Gradient, Pattern, Ring, Stripes};
use crate::properties::{Color, Material, WHITE};
use crate::scene::Scene;
use crate::shapes::{Cube, Cylinder, Plane, Shape, ShapeProperties, Sphere};
use math::{Matrix4f, Point4f, Vector4f};
use std::error::Error;
use std::fmt::{Display, Formatter};
use yaml_rust::yaml::Array;
use yaml_rust::{Yaml, YamlLoader};

/// Scene parse errors.
#[derive(Debug, PartialEq)]
pub enum ParseError {
    /// A mandatory mapping is missing.
    MissingMapping,
    /// Invalid mapping type.
    InvalidMapping,
    /// An array element has invalid type.
    /// ```text
    /// InvalidElementType(index)
    /// ```
    InvalidElementType(usize),
    /// Invalid array size for a mapping.
    /// ```text
    /// InvalidArraySize(expected size, actual size).
    /// ```
    InvalidArraySize(usize, usize),
    /// The syntax of the yaml file is invalid.
    InvalidYamlSyntax,
    /// A ParseError with a tag.
    TagChain(String, Box<ParseError>),
}

impl Display for ParseError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ParseError::InvalidYamlSyntax => write!(f, "Yaml parse error"),
            ParseError::MissingMapping => write!(f, "Missing mapping"),
            ParseError::InvalidMapping => write!(f, "Invalid mapping"),
            ParseError::InvalidElementType(index) => {
                write!(f, "Invalid element type at index {}", index)
            }
            ParseError::InvalidArraySize(expected, actual) => write!(
                f,
                "Invalid array size. Expected size: {}, Actual size: {}",
                expected, actual
            ),
            ParseError::TagChain(mapping, error) => write!(f, "{}: {}", mapping, error),
        }
    }
}

impl ParseError {
    /// Adds a tag to a [ParseError].
    fn chain(self, mapping: &str) -> Self {
        ParseError::TagChain(mapping.into(), Box::new(self))
    }
}

impl Error for ParseError {}

/// Converts a 3 element [array][yaml_rust::yaml::Array] to another type.
macro_rules! define_as_array3 {
    ($name: ident, $t: ty) => {
        fn $name(&self) -> Result<$t, ParseError> {
            if self.len() != 3 {
                Err(ParseError::InvalidArraySize(3, self.len()))?
            }

            Ok(<$t>::new(
                self[0]
                    .as_float()
                    .ok_or(ParseError::InvalidElementType(0))?,
                self[1]
                    .as_float()
                    .ok_or(ParseError::InvalidElementType(1))?,
                self[2]
                    .as_float()
                    .ok_or(ParseError::InvalidElementType(2))?,
            ))
        }
    };
}

/// Converts an array to a rotate transformation.
macro_rules! define_as_rotate {
    ($name: ident, $function: ident) => {
        fn $name(&self) -> Result<Matrix4f, ParseError> {
            if self.len() != 2 {
                Err(ParseError::InvalidArraySize(2, self.len()))?
            }

            let radians = self[1]
                .as_float()
                .ok_or(ParseError::InvalidElementType(1))?;
            Ok(Matrix4f::identity().$function(radians))
        }
    };
}

/// Functions that convert an [array][yaml_rust::yaml::Array] to another type.
trait ArrayConverters {
    /// Converts a 3 element [array][yaml_rust::yaml::Array] to a point.
    fn as_point4f(&self) -> Result<Point4f, ParseError>;

    /// Converts a 3 element [array][yaml_rust::yaml::Array] to a vector.
    fn as_vector4f(&self) -> Result<Vector4f, ParseError>;

    /// Converts a 3 element [array][yaml_rust::yaml::Array] to a color.
    fn as_color(&self) -> Result<Color, ParseError>;

    /// Parses a [scale][Matrix4f::scale] transformation.
    fn as_scale(&self) -> Result<Matrix4f, ParseError>;

    /// Parses a [translate][Matrix4f::translate] transformation.
    fn as_translate(&self) -> Result<Matrix4f, ParseError>;

    /// Parses a [shearing][Matrix4f::shearing] transformation.
    fn as_shearing(&self) -> Result<Matrix4f, ParseError>;

    /// Parses a [x axis rotation][Matrix4f::rotate_x] transformation.
    fn as_rotate_x(&self) -> Result<Matrix4f, ParseError>;

    /// Parses a [y axis rotation][Matrix4f::rotate_y] transformation.
    fn as_rotate_y(&self) -> Result<Matrix4f, ParseError>;

    /// Parses a [z axis rotation][Matrix4f::rotate_z] transformation.
    fn as_rotate_z(&self) -> Result<Matrix4f, ParseError>;
}

impl ArrayConverters for Array {
    define_as_array3!(as_point4f, Point4f);
    define_as_array3!(as_vector4f, Vector4f);
    define_as_array3!(as_color, Color);
    define_as_rotate!(as_rotate_x, rotate_x);
    define_as_rotate!(as_rotate_y, rotate_y);
    define_as_rotate!(as_rotate_z, rotate_z);

    fn as_scale(self: &Array) -> Result<Matrix4f, ParseError> {
        if self.len() != 4 {
            Err(ParseError::InvalidArraySize(4, self.len()))?
        }

        let x = self[1]
            .as_float()
            .ok_or(ParseError::InvalidElementType(1))?;
        let y = self[2]
            .as_float()
            .ok_or(ParseError::InvalidElementType(2))?;
        let z = self[3]
            .as_float()
            .ok_or(ParseError::InvalidElementType(3))?;
        Ok(Matrix4f::identity().scale(x, y, z))
    }

    fn as_translate(self: &Array) -> Result<Matrix4f, ParseError> {
        if self.len() != 4 {
            Err(ParseError::InvalidArraySize(4, self.len()))?
        }

        let x = self[1]
            .as_float()
            .ok_or(ParseError::InvalidElementType(1))?;
        let y = self[2]
            .as_float()
            .ok_or(ParseError::InvalidElementType(2))?;
        let z = self[3]
            .as_float()
            .ok_or(ParseError::InvalidElementType(3))?;
        Ok(Matrix4f::identity().translate(x, y, z))
    }

    fn as_shearing(&self) -> Result<Matrix4f, ParseError> {
        if self.len() != 7 {
            Err(ParseError::InvalidArraySize(7, self.len()))?
        }

        let xy = self[1]
            .as_float()
            .ok_or(ParseError::InvalidElementType(1))?;
        let xz = self[2]
            .as_float()
            .ok_or(ParseError::InvalidElementType(2))?;
        let yx = self[3]
            .as_float()
            .ok_or(ParseError::InvalidElementType(3))?;
        let yz = self[4]
            .as_float()
            .ok_or(ParseError::InvalidElementType(4))?;
        let zx = self[5]
            .as_float()
            .ok_or(ParseError::InvalidElementType(5))?;
        let zy = self[6]
            .as_float()
            .ok_or(ParseError::InvalidElementType(6))?;
        Ok(Matrix4f::identity().shearing(xy, xz, yx, yz, zx, zy))
    }
}

/// Functions that convert a [yaml node][Yaml] to another type.
trait YamlConverters {
    /// Converts an [Integer][Yaml::Integer] yaml node to usize.
    fn as_usize(&self) -> Option<usize>;

    /// Converts [Integer][Yaml::Integer] and [Real][Yaml::Real] yaml nodes to f64.
    fn as_float(&self) -> Option<f64>;
}

impl YamlConverters for Yaml {
    fn as_usize(&self) -> Option<usize> {
        match self.as_i64() {
            Some(n) if n > 0 => Some(n as usize),
            _ => None,
        }
    }

    fn as_float(&self) -> Option<f64> {
        match *self {
            Yaml::Integer(v) => Some(v as f64),
            _ => self.as_f64(),
        }
    }
}

/// Functions that parse yaml mappings.
trait MappingParsers {
    /// Returns an error if the mapping is missing.
    fn check_mapping(&self, mapping: &str) -> Result<(), ParseError>;

    /// Parses a yaml mapping as a color.
    fn parse_color(&self, mapping: &str) -> Result<Color, ParseError>;

    /// Parses a yaml mapping as a point.
    fn parse_point4f(&self, mapping: &str) -> Result<Point4f, ParseError>;

    /// Parses a yaml mapping as a vector.
    fn parse_vector4f(&self, mapping: &str) -> Result<Vector4f, ParseError>;

    /// Parses a yaml mapping as a bool.
    fn parse_bool(&self, mapping: &str) -> Result<bool, ParseError>;

    /// Parses a yaml mapping as a usize.
    fn parse_usize(&self, mapping: &str) -> Result<usize, ParseError>;

    /// Parses a yaml mapping as a f64.
    fn parse_float(&self, mapping: &str) -> Result<f64, ParseError>;

    /// Parses a yaml mapping as a str.
    fn parse_str(&self, mapping: &str) -> Result<&str, ParseError>;
}

/// Parses a vector mapping.
macro_rules! define_parse_vec {
    ($name: ident, $function: ident, $t: ty) => {
        fn $name(&self, mapping: &str) -> Result<$t, ParseError> {
            self.check_mapping(mapping)?;
            self[mapping]
                .as_vec()
                .ok_or_else(|| ParseError::InvalidMapping.chain(mapping))?
                .$function()
                .map_err(|e| e.chain(mapping))
        }
    };
}

/// Parses a simple type mapping.
macro_rules! define_parse_type {
    ($name: ident, $function: ident, $t: ty) => {
        fn $name(&self, mapping: &str) -> Result<$t, ParseError> {
            self.check_mapping(mapping)?;
            self[mapping]
                .$function()
                .ok_or_else(|| ParseError::InvalidMapping.chain(mapping))
        }
    };
}

impl MappingParsers for Yaml {
    define_parse_vec!(parse_color, as_color, Color);
    define_parse_vec!(parse_point4f, as_point4f, Point4f);
    define_parse_vec!(parse_vector4f, as_vector4f, Vector4f);
    define_parse_type!(parse_bool, as_bool, bool);
    define_parse_type!(parse_usize, as_usize, usize);
    define_parse_type!(parse_float, as_float, f64);
    define_parse_type!(parse_str, as_str, &str);

    fn check_mapping(&self, mapping: &str) -> Result<(), ParseError> {
        if self[mapping].is_badvalue() {
            Err(ParseError::MissingMapping.chain(mapping))
        } else {
            Ok(())
        }
    }
}

pub struct SceneParser {
    scene: Scene,
    camera: Option<Camera>,
}

impl SceneParser {
    /// Creates a new SceneParser.
    fn new() -> Self {
        Self {
            scene: Scene::new(),
            camera: None,
        }
    }

    /// Parses a yaml string and returns a Camera and a Scene.
    pub fn load(yaml: &str) -> Result<(Camera, Scene), ParseError> {
        let docs = YamlLoader::load_from_str(yaml).map_err(|_| ParseError::InvalidYamlSyntax)?;
        let mut scene_parser = SceneParser::new();
        scene_parser.parse(&docs[0])?;

        if let Some(camera) = scene_parser.camera {
            Ok((camera, scene_parser.scene))
        } else {
            Err(ParseError::MissingMapping.chain("camera"))
        }
    }

    /// Parses a yaml document.
    fn parse(&mut self, doc: &Yaml) -> Result<(), ParseError> {
        if let Some(arr) = doc.as_vec() {
            for value in arr {
                if !value["add"].is_badvalue() {
                    self.parse_add(value)?
                }
            }
            Ok(())
        } else {
            Err(ParseError::InvalidMapping)
        }
    }

    /// Parses add mappings.
    fn parse_add(&mut self, yaml: &Yaml) -> Result<(), ParseError> {
        match yaml["add"].as_str().unwrap() {
            "camera" => {
                self.camera = Some(SceneParser::parse_camera(yaml).map_err(|e| e.chain("camera"))?);
            }
            "light" => {
                self.scene
                    .add_light(SceneParser::parse_light(yaml).map_err(|e| e.chain("light"))?);
            }
            obj => {
                self.scene
                    .add_shape(SceneParser::parse_shape(yaml).map_err(|e| e.chain(obj))?);
            }
        };

        Ok(())
    }

    /// Parses a camera section and creates a camera.
    ///
    /// # Camera section format
    /// ```yaml
    /// - add: camera
    ///   width: int                        # Mandatory field.
    ///   height: int                       # Mandatory field.
    ///   field_of_view: float              # Mandatory field.
    ///   from: [float, float, float]       # Mandatory field.
    ///   to: [float, float, float]         # Mandatory field.
    ///   up: [float, float, float]         # Mandatory field.
    /// ```
    fn parse_camera(yaml: &Yaml) -> Result<Camera, ParseError> {
        let width = yaml.parse_usize("width")?;
        let height = yaml.parse_usize("height")?;
        let field_of_view = yaml.parse_float("field_of_view")?;
        let from = yaml.parse_point4f("from")?;
        let to = yaml.parse_point4f("to")?;
        let up = yaml.parse_vector4f("up")?;

        let mut camera = Camera::new(width, height, field_of_view);
        camera.transform_view(from, to, up);

        Ok(camera)
    }

    /// Parses a light section and creates a light.
    ///
    /// # Light section format
    /// ```yaml
    /// - add: light
    ///   at: [float, float, float]         # Mandatory field.
    ///   intensity: [float, float, float]  # Mandatory field.
    /// ```
    fn parse_light(yaml: &Yaml) -> Result<Box<dyn Light>, ParseError> {
        let position = yaml.parse_point4f("at")?;
        let intensity = yaml.parse_color("intensity")?;

        Ok(Box::new(PointLight::with_intensity(position, intensity)))
    }

    /// Parses a shape section and creates a shape.
    ///
    /// # Shape section format
    /// ```yaml
    /// - add: [sphere, plane]
    ///   transform:                        # Transform section. Optional field.
    ///     ...
    ///   material:                         # Material section. Optional field.
    ///     ...
    ///   pattern:                          # Pattern section. Optional field.
    ///     ...
    /// ```
    fn parse_shape(yaml: &Yaml) -> Result<Box<dyn Shape>, ParseError> {
        let shape_properties = SceneParser::parse_shape_properties(yaml)?;

        match yaml["add"].as_str().unwrap() {
            "sphere" => Ok(Box::new(Sphere::new(shape_properties))),
            "plane" => Ok(Box::new(Plane::new(shape_properties))),
            "cube" => Ok(Box::new(Cube::new(shape_properties))),
            "cylinder" => {
                let (minimum, maximum, closed) = SceneParser::parse_cylinder_properties(yaml)?;
                Ok(Box::new(Cylinder::with_limits(
                    shape_properties,
                    minimum,
                    maximum,
                    closed,
                )))
            }
            _ => Err(ParseError::InvalidMapping)?,
        }
    }

    /// Parses the material, the pattern and the transformation of a shape.
    fn parse_shape_properties(yaml: &Yaml) -> Result<ShapeProperties, ParseError> {
        let pattern = if !yaml["pattern"].is_badvalue() {
            SceneParser::parse_pattern(&yaml["pattern"]).map_err(|e| e.chain("pattern"))?
        } else {
            Box::new(WHITE)
        };
        let material = if !yaml["material"].is_badvalue() {
            SceneParser::parse_material(&yaml["material"]).map_err(|e| e.chain("material"))?
        } else {
            Material::default()
        };
        let transform = if !yaml["transform"].is_badvalue() {
            SceneParser::parse_transform(&yaml["transform"]).map_err(|e| e.chain("shape"))?
        } else {
            Matrix4f::identity()
        };

        Ok(ShapeProperties::with_transform(
            pattern, material, transform,
        ))
    }

    /// Parses the properties of a cylinder.
    ///
    /// # Cylinder properties
    /// ```yaml
    ///   min: float                        # Optional field.
    ///   max: float                        # Optional field.
    ///   closed: bool                      # Optional field.
    /// ```
    fn parse_cylinder_properties(yaml: &Yaml) -> Result<(f64, f64, bool), ParseError> {
        let minimum = if !yaml["min"].is_badvalue() {
            yaml.parse_float("min")?
        } else {
            -f64::INFINITY
        };
        let maximum = if !yaml["max"].is_badvalue() {
            yaml.parse_float("max")?
        } else {
            f64::INFINITY
        };
        let closed = if !yaml["closed"].is_badvalue() {
            yaml.parse_bool("closed")?
        } else {
            false
        };

        Ok((minimum, maximum, closed))
    }

    /// Parses a material section and creates a material.
    ///
    /// # Material section format
    /// ```yaml
    /// material:
    ///   ambient: float                    # Optional field.
    ///   diffuse: float                    # Optional field.
    ///   specular: float                   # Optional field.
    ///   shininess: float                  # Optional field.
    ///   reflective: float                 # Optional field.
    ///   transparency: float               # Optional field.
    ///   refractive_index: float           # Optional field.
    /// ```
    fn parse_material(yaml: &Yaml) -> Result<Material, ParseError> {
        let mut material = Material::default();
        SceneParser::parse_material_field(yaml, "ambient", &mut material.ambient)?;
        SceneParser::parse_material_field(yaml, "diffuse", &mut material.diffuse)?;
        SceneParser::parse_material_field(yaml, "specular", &mut material.specular)?;
        SceneParser::parse_material_field(yaml, "shininess", &mut material.shininess)?;
        SceneParser::parse_material_field(yaml, "reflective", &mut material.reflective)?;
        SceneParser::parse_material_field(yaml, "transparency", &mut material.transparency)?;
        SceneParser::parse_material_field(
            yaml,
            "refractive_index",
            &mut material.refractive_index,
        )?;

        Ok(material)
    }

    fn parse_material_field(yaml: &Yaml, mapping: &str, field: &mut f64) -> Result<(), ParseError> {
        if !yaml[mapping].is_badvalue() {
            *field = yaml.parse_float(mapping)?;
        }
        Ok(())
    }

    /// Parses a transform section and create a transformation matrix. A transform section may have
    /// one or more transformations.
    ///
    /// # Transform section format:
    /// ```yaml
    /// transform:
    ///   - [scale, float, float, float]
    ///   - [rotate_x, float]
    ///   - [rotate_y, float]
    ///   - [rotate_z, float]
    ///   - [translate, float, float, float]
    ///   - [shearing, float, float, float, float, float, float]
    /// ```
    ///
    /// # Transforms
    /// ## Scale
    /// ```yaml
    /// [scale, x, y, z]
    /// ```
    ///
    /// ## Shearing
    /// ```yaml
    /// [shearing, xy, xz, yx, yz, zx, zy]
    /// ```
    ///
    /// ## Rotate around x axis
    /// ```yaml
    /// [rotate_x, radians]
    /// ```
    ///
    /// ## Rotate around y axis
    /// ```yaml
    /// [rotate_y, radians]
    /// ```
    ///
    /// ## Rotate around z axis
    /// ```yaml
    /// [rotate_z, radians]
    /// ```
    fn parse_transform(yaml: &Yaml) -> Result<Matrix4f, ParseError> {
        let mut transform = Matrix4f::identity();
        for array in yaml
            .as_vec()
            .ok_or_else(|| ParseError::InvalidMapping.chain("transform"))?
        {
            let array = array
                .as_vec()
                .ok_or_else(|| ParseError::InvalidMapping.chain("transform"))?;

            let transform_type = array[0]
                .as_str()
                .ok_or_else(|| ParseError::InvalidMapping.chain("transform type"))?;

            let next_transform = match transform_type {
                "rotate_x" => array.as_rotate_x().map_err(|e| e.chain("rotate_x"))?,
                "rotate_y" => array.as_rotate_y().map_err(|e| e.chain("rotate_y"))?,
                "rotate_z" => array.as_rotate_z().map_err(|e| e.chain("rotate_z"))?,
                "translate" => array.as_translate().map_err(|e| e.chain("translate"))?,
                "shearing" => array.as_shearing().map_err(|e| e.chain("shearing"))?,
                "scale" => array.as_scale().map_err(|e| e.chain("scale"))?,
                _ => Matrix4f::identity(),
            };
            transform = &next_transform * &transform;
        }

        Ok(transform)
    }

    /// Parses a pattern section and creates a pattern.
    ///
    /// # Pattern section format
    ///
    /// ## Solid color pattern
    /// ```yaml
    /// pattern:
    ///   color: [float, float, float]      # The color range is [0, 1].
    /// ```
    ///
    /// ## Patterns that require two sub-patterns
    ///
    /// ```yaml
    /// pattern:
    ///   type: [stripes | checkers | ring | gradient]
    ///   transform:                        # Transform section. Optional field.
    ///     ...
    ///   pattern1:                         # Pattern section.
    ///     color: [float, float, float]
    ///   pattern2:                         # Pattern section.
    ///     color: [float, float, float]
    /// ```
    ///
    /// ## Nested patterns
    /// There is no limit in nested patterns depth. The leaf node is color.
    /// ```yaml
    /// pattern:
    ///   type: [stripes | checkers | ring | gradient]
    ///   transform:                        # Transform section. Optional field.
    ///     ...
    ///   pattern1:                         # Pattern section.
    ///     type: [stripes | checkers | ring | gradient]
    ///     transform:                      # Transform section. Optional field.
    ///       ...
    ///     pattern1:                       # Pattern section.
    ///       color: [float, float, float]
    ///     pattern2:                       # Pattern section.
    ///       color: [float, float, float]
    ///   pattern2:                         # Pattern section.
    ///     color: [float, float, float]
    /// ```
    fn parse_pattern(yaml: &Yaml) -> Result<Box<dyn Pattern>, ParseError> {
        if yaml["type"].is_badvalue() && !yaml["color"].is_badvalue() {
            let color = yaml.parse_color("color")?;
            return Ok(Box::new(color));
        }

        let transform = if !yaml["transform"].is_badvalue() {
            SceneParser::parse_transform(&yaml["transform"])?
        } else {
            Matrix4f::identity()
        };

        let pattern1 = SceneParser::parse_pattern(&yaml["pattern1"])?;
        let pattern2 = SceneParser::parse_pattern(&yaml["pattern2"])?;
        let pattern_type = yaml.parse_str("type")?;
        Ok(match pattern_type {
            "stripes" => Box::new(Stripes::with_transform(pattern1, pattern2, transform)),
            "checkers" => Box::new(Checkers::with_transform(pattern1, pattern2, transform)),
            "gradient" => Box::new(Gradient::with_transform(pattern1, pattern2, transform)),
            "ring" => Box::new(Ring::with_transform(pattern1, pattern2, transform)),
            _ => Err(ParseError::InvalidMapping.chain("type"))?,
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::camera::Camera;
    use crate::pattern::{Checkers, Gradient, Pattern, Ring, Stripes};
    use crate::properties::{Color, Material, WHITE};
    use crate::scene_parser::{ArrayConverters, MappingParsers, SceneParser, YamlConverters};
    use math::{Matrix4f, Point4f, Vector4f};
    use yaml_rust::YamlLoader;

    #[test]
    fn array_converters() {
        let yaml_str = "
  vector: [1, 2, 3]
  point: [1.2, 2.2, 3.3]
  color: [0.2, 0.2, 0.1]
  scale: [scale, 0.5, 0.5, 0.3]
  translate: [translate, 2, 3, 1.5]
  shearing: [shearing, 5, 1, 3, 2, 1, 2]
  rotate_x: [rotate_x, 1.2]
  rotate_y: [rotate_y, 1.5]
  rotate_z: [rotate_z, 2]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        assert_eq!(
            doc["vector"].as_vec().unwrap().as_vector4f().unwrap(),
            Vector4f::new(1., 2., 3.)
        );
        assert_eq!(
            doc["point"].as_vec().unwrap().as_point4f().unwrap(),
            Point4f::new(1.2, 2.2, 3.3)
        );
        assert_eq!(
            doc["color"].as_vec().unwrap().as_color().unwrap(),
            Color::new(0.2, 0.2, 0.1)
        );
        assert_eq!(
            doc["scale"].as_vec().unwrap().as_scale().unwrap(),
            Matrix4f::identity().scale(0.5, 0.5, 0.3)
        );
        assert_eq!(
            doc["translate"].as_vec().unwrap().as_translate().unwrap(),
            Matrix4f::identity().translate(2., 3., 1.5)
        );
        assert_eq!(
            doc["shearing"].as_vec().unwrap().as_shearing().unwrap(),
            Matrix4f::identity().shearing(5., 1., 3., 2., 1., 2.)
        );
        assert_eq!(
            doc["rotate_x"].as_vec().unwrap().as_rotate_x().unwrap(),
            Matrix4f::identity().rotate_x(1.2)
        );
        assert_eq!(
            doc["rotate_y"].as_vec().unwrap().as_rotate_y().unwrap(),
            Matrix4f::identity().rotate_y(1.5)
        );
        assert_eq!(
            doc["rotate_z"].as_vec().unwrap().as_rotate_z().unwrap(),
            Matrix4f::identity().rotate_z(2.)
        );
    }

    #[test]
    fn yaml_converters() {
        let yaml_str = "
  usize: 2
  usize_err: -1
  real: 2.2
  integer: 1
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        assert_eq!(doc["usize"].as_usize().unwrap(), 2);
        assert_eq!(doc["usize_err"].as_usize(), None);
        assert_eq!(doc["real"].as_float().unwrap(), 2.2);
        assert_eq!(doc["integer"].as_float().unwrap(), 1.);
    }

    #[test]
    fn mapping_parsers() {
        let yaml_str = "
  bool: true
  usize: 1
  float: 1.5
  str: string
  color: [1, 1, 1]
  point: [0.2, 0.2, 0.1]
  vector: [1.5, 2, 3.5]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        assert_eq!(doc.parse_bool("bool").unwrap(), true);
        assert_eq!(doc.parse_usize("usize").unwrap(), 1);
        assert_eq!(doc.parse_float("float").unwrap(), 1.5);
        assert_eq!(doc.parse_str("str").unwrap(), "string");
        assert_eq!(doc.parse_color("color").unwrap(), Color::new(1., 1., 1.));
        assert_eq!(
            doc.parse_point4f("point").unwrap(),
            Point4f::new(0.2, 0.2, 0.1)
        );
        assert_eq!(
            doc.parse_vector4f("vector").unwrap(),
            Vector4f::new(1.5, 2., 3.5)
        );
    }

    #[test]
    fn load() {
        let yaml_str = "
- add: camera
  width: 10
  height: 10
  field_of_view: 1.2
  from: [0.0, 1.5, -5.0]
  to: [0, 1.0, 0.0]
  up: [0.0, 1.0, 0.0]

- add: light
  at: [-10.0, 10.0, -10.0]
  intensity: [1.0, 1.0, 1.0]

- add: plane
  material:
    reflective: 0.95
  pattern:
    type: checkers
    pattern1:
      color: [1.0, 1.0, 1.0]
    pattern2:
      color: [0.0, 0.0, 0.0]

- add: sphere
  pattern:
    type: stripes
    transform:
      - [rotate_z, 1.1]
    pattern1:
      color: [0, 0, 1]
    pattern2:
      color: [1, 1, 1]
";
        let (camera, scene) = SceneParser::load(yaml_str).unwrap();
        let canvas = camera.render(&scene);

        assert_eq!(
            String::from_utf8(canvas.to_ppm(Vec::new()).unwrap()).unwrap(),
            "P3
10 10
255
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 67 67 67 0 0 0 
0 0 0 66 66 66 65 65 65 0 0 0 61 61 61 59 59 59 0 0 0 153 153 153 
0 0 0 0 0 0 0 0 241 0 0 152 135 135 135 132 132 132 0 0 0 124 124 124 
0 0 0 169 169 169 0 0 0 0 0 0 0 0 212 0 0 130 25 25 25 151 151 151 
0 0 0 145 145 145 0 0 0 174 174 174 171 171 171 0 0 0 0 0 218 
164 164 255 161 161 161 0 0 0 0 0 0 154 154 154 0 0 0 0 0 0 
174 174 174 172 172 172 170 170 170 0 0 0 0 0 0 0 0 0 162 162 162 
160 160 160 178 178 178 177 177 177 0 0 0 0 0 0 0 0 0 170 170 170 
168 168 168 166 166 166 0 0 0 0 0 0 "
        );
    }

    #[test]
    fn parse_camera() {
        let yaml_str = "
  width: 720
  height: 720
  field_of_view: 1.2
  from: [0.0, 1.5, -5.0]
  to: [0, 1.0, 0.0]
  up: [0.0, 1.0, 0.0]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        let mut expected_camera = Camera::new(720, 720, 1.2);
        expected_camera.transform_view(
            Point4f::new(0., 1.5, -5.),
            Point4f::new(0., 1., 0.),
            Vector4f::new(0., 1., 0.),
        );

        assert_eq!(SceneParser::parse_camera(doc).unwrap(), expected_camera);
    }

    #[test]
    fn parse_light() {
        let yaml_str = "
  at: [2, -1, 0]
  intensity: [0.2, 0.5, 0.7]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        // Tests light's position.
        let expected_position = Point4f::new(2., -1., 0.);
        let parsed_light = SceneParser::parse_light(doc).unwrap();
        assert_eq!(parsed_light.get_position(), &expected_position);

        // Tests light's intensity.
        let point = Point4f::new(0., 0., 0.);
        let vector = Vector4f::new(0., 0., 0.);
        let material = Material {
            ambient: 1.,
            ..Material::default()
        };
        assert_eq!(
            parsed_light.lighting(&point, &vector, &vector, &material, WHITE, true),
            Color::new(0.2, 0.5, 0.7)
        );
    }

    #[test]
    fn parse_cylinder_properties() {
        let yaml_str = "
  min: 1.4
  max: 2
  closed: true
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        let (min, max, closed) = SceneParser::parse_cylinder_properties(doc).unwrap();
        assert_eq!(min, 1.4);
        assert_eq!(max, 2.);
        assert_eq!(closed, true);
    }

    #[test]
    fn parse_material() {
        let yaml_str = "
  ambient: 0.5
  diffuse: 0.2
  specular: 0.3
  shininess: 100
  reflective: 0.9
  refractive_index: 0.7
  transparency: 0.2
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        assert_eq!(
            SceneParser::parse_material(doc).unwrap(),
            Material {
                ambient: 0.5,
                diffuse: 0.2,
                specular: 0.3,
                shininess: 100.,
                reflective: 0.9,
                refractive_index: 0.7,
                transparency: 0.2
            }
        )
    }

    #[test]
    fn parse_transform() {
        let yaml_str = "
  - [scale, 0.2, 0.3, 0.1]
  - [translate, 1, 2, 3]
  - [shearing, 1, 2, 3, 4, 5, 6]
  - [rotate_x, 0.5]
  - [rotate_y, 1]
  - [rotate_z, 1.5]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        assert_eq!(
            SceneParser::parse_transform(doc).unwrap(),
            Matrix4f::identity()
                .scale(0.2, 0.3, 0.1)
                .translate(1., 2., 3.)
                .shearing(1., 2., 3., 4., 5., 6.)
                .rotate_x(0.5)
                .rotate_y(1.)
                .rotate_z(1.5)
        );
    }

    #[test]
    fn parse_pattern() {
        let yaml_str = "
  type: stripes
  pattern1:
    type: checkers
    transform:
      - [scale, 0.5, 0.5, 0.5]
    pattern1:
      color: [0, 1, 0]
    pattern2:
      type: ring
      transform:
        - [scale, 0.1, 0.1, 0.1]
      pattern1:
        color: [1, 0.5, 0]
      pattern2:
        color: [0.5, 0, 0]
  pattern2:
    type: gradient
    pattern1:
      color: [0.1, 0.7, 0.1]
    pattern2:
      color: [0.1, 0.2, 0.3]
";
        let yaml = YamlLoader::load_from_str(yaml_str).unwrap();
        let doc = &yaml[0];

        let ring_pattern = Ring::with_transform(
            Box::new(Color::new(1., 0.5, 0.)),
            Box::new(Color::new(0.5, 0., 0.)),
            Matrix4f::identity().scale(0.1, 0.1, 0.1),
        );
        let checkers_pattern = Checkers::with_transform(
            Box::new(Color::new(0., 1., 0.)),
            Box::new(ring_pattern),
            Matrix4f::identity().scale(0.5, 0.5, 0.5),
        );
        let gradient_pattern = Gradient::new(
            Box::new(Color::new(0.1, 0.7, 0.1)),
            Box::new(Color::new(0.1, 0.2, 0.3)),
        );
        let expected_pattern = Stripes::new(Box::new(checkers_pattern), Box::new(gradient_pattern));
        let parsed_pattern = SceneParser::parse_pattern(doc).unwrap();

        let points = vec![
            Point4f::new(0.1, 0.1, 0.1),
            Point4f::new(0.51, 0.51, 0.51),
            Point4f::new(0.55, 0.55, 0.55),
            Point4f::new(1.1, 1.1, 1.1),
        ];

        points
            .iter()
            .for_each(|p| assert_eq!(parsed_pattern.color_at(p), expected_pattern.color_at(p)));
    }
}
