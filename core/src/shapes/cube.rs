use crate::ray::Ray;
use crate::shapes::shape::ShapeComputations;
use crate::shapes::{IntersectionRecorder, Shape, ShapeProperties};
use math::utils::EPSILON;
use math::{Point4f, Vector4f};

/// An axis aligned cube that is centered the world origin (0, 0, 0) and extends
/// from -1 to 1 along each axis.
pub struct Cube {
    properties: ShapeProperties,
}

impl Cube {
    /// Creates a new cube.
    pub fn new(properties: ShapeProperties) -> Self {
        Self { properties }
    }

    fn check_axis(origin: f64, direction: f64) -> (f64, f64) {
        let tmin_numerator = -1. - origin;
        let tmax_numerator = 1. - origin;

        let (tmin, tmax) = if direction.abs() >= EPSILON {
            (tmin_numerator / direction, tmax_numerator / direction)
        } else {
            (
                tmin_numerator * f64::INFINITY,
                tmax_numerator * f64::INFINITY,
            )
        };

        if tmin > tmax {
            return (tmax, tmin);
        }

        (tmin, tmax)
    }
}

impl Shape for Cube {}

impl ShapeComputations for Cube {
    fn compute_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersection_recorder: &mut IntersectionRecorder<'a>,
    ) {
        let (origin, direction) = (ray.get_origin(), ray.get_direction());
        let (x_tmin, x_tmax) = Cube::check_axis(origin.x, direction.x);
        let (y_tmin, y_tmax) = Cube::check_axis(origin.y, direction.y);
        let (z_tmin, z_tmax) = Cube::check_axis(origin.z, direction.z);

        let tmin = f64::max(x_tmin, f64::max(y_tmin, z_tmin));
        let tmax = f64::min(x_tmax, f64::min(y_tmax, z_tmax));

        if tmin <= tmax {
            intersection_recorder.add(self, tmin);
            intersection_recorder.add(self, tmax);
        }
    }

    fn compute_normal(&self, point: &Point4f) -> Vector4f {
        let maxc = f64::max(point.x.abs(), f64::max(point.y.abs(), point.z.abs()));

        if maxc == point.x.abs() {
            return Vector4f::new(point.x, 0., 0.);
        } else if maxc == point.y.abs() {
            return Vector4f::new(0., point.y, 0.);
        }
        Vector4f::new(0., 0., point.z)
    }

    fn get_properties(&self) -> &ShapeProperties {
        &self.properties
    }
}

#[cfg(test)]
mod tests {
    use crate::ray::Ray;
    use crate::shapes::shape::ShapeComputations;
    use crate::shapes::{Cube, IntersectionRecorder, ShapeProperties};
    use math::{Point4f, Vector4f};

    #[test]
    fn compute_intersections() {
        let origin_points = [
            Point4f::new(5., 0.5, 0.),
            Point4f::new(-5., 0.5, 0.),
            Point4f::new(0.5, 5., 0.),
            Point4f::new(0.5, -5., 0.),
            Point4f::new(0.5, 0., 5.),
            Point4f::new(0.5, 0., -5.),
            Point4f::new(0., 0.5, 0.),
        ];

        let direction_vectors = [
            Vector4f::new(-1., 0., 0.),
            Vector4f::new(1., 0., 0.),
            Vector4f::new(0., -1., 0.),
            Vector4f::new(0., 1., 0.),
            Vector4f::new(0., 0., -1.),
            Vector4f::new(0., 0., 1.),
            Vector4f::new(0., 0., 1.),
        ];

        let expected_intersections = [
            [4., 6.],
            [4., 6.],
            [4., 6.],
            [4., 6.],
            [4., 6.],
            [4., 6.],
            [-1., 1.],
        ];

        let cube = Cube::new(ShapeProperties::default());
        origin_points
            .into_iter()
            .zip(direction_vectors)
            .zip(expected_intersections)
            .for_each(|((origin, direction), expected_intersections)| {
                let ray = Ray::new(origin, direction);
                let mut intersection_recorder = IntersectionRecorder::new();
                cube.compute_intersections(&ray, &mut intersection_recorder);

                let intersections: Vec<f64> = intersection_recorder
                    .sorted()
                    .iter()
                    .map(|intersection| intersection.t)
                    .collect();
                assert_eq!(intersections, expected_intersections);
            });
    }

    #[test]
    fn compute_intersections_miss() {
        let origin_points = [
            Point4f::new(-2., 0., 0.),
            Point4f::new(0., -2., 0.),
            Point4f::new(0., 0., -2.),
            Point4f::new(2., 0., 2.),
            Point4f::new(0., 2., 2.),
            Point4f::new(2., 2., 0.),
        ];

        let direction_vectors = [
            Vector4f::new(0.2673, 0.5345, 0.8018),
            Vector4f::new(0.8018, 0.2673, 0.5345),
            Vector4f::new(0.5345, 0.8018, 0.2673),
            Vector4f::new(0., 0., -1.),
            Vector4f::new(0., -1., 0.),
            Vector4f::new(-1., 0., 0.),
        ];

        let cube = Cube::new(ShapeProperties::default());
        origin_points
            .into_iter()
            .zip(direction_vectors)
            .for_each(|(origin, direction)| {
                let ray = Ray::new(origin, direction);
                let mut intersection_recorder = IntersectionRecorder::new();
                cube.compute_intersections(&ray, &mut intersection_recorder);
                assert_eq!(intersection_recorder.len(), 0);
            });
    }

    #[test]
    fn compute_normal() {
        let points = [
            Point4f::new(1., 0.5, -0.8),
            Point4f::new(-1., -0.2, 0.9),
            Point4f::new(-0.4, 1., -0.1),
            Point4f::new(0.3, -1., -0.7),
            Point4f::new(-0.6, 0.3, 1.),
            Point4f::new(0.4, 0.4, -1.),
            Point4f::new(1., 1., 1.),
            Point4f::new(-1., -1., -1.),
        ];

        let expected_normals = [
            Vector4f::new(1., 0., 0.),
            Vector4f::new(-1., 0., 0.),
            Vector4f::new(0., 1., 0.),
            Vector4f::new(0., -1., 0.),
            Vector4f::new(0., 0., 1.),
            Vector4f::new(0., 0., -1.),
            Vector4f::new(1., 0., 0.),
            Vector4f::new(-1., 0., 0.),
        ];

        let cube = Cube::new(ShapeProperties::default());
        points
            .iter()
            .zip(expected_normals)
            .for_each(|(point, expected_normal)| {
                assert_eq!(cube.compute_normal(point), expected_normal)
            });
    }
}
