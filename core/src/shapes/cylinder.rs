use crate::ray::Ray;
use crate::shapes::shape::ShapeComputations;
use crate::shapes::{IntersectionRecorder, Shape, ShapeProperties};
use math::utils::EPSILON;
use math::{Point4f, Vector4f};

/// A vertical cylinder that is centered at the world origin (0, 0, 0) and extends
/// from minimum to maximum.
pub struct Cylinder {
    /// Bottom limit. The cylinder extends up to - but not including - this limit.
    minimum: f64,
    /// Top limit. The cylinder extends up to - but not including - this limit.
    maximum: f64,
    /// Whether the cylinder is capped or uncapped.
    closed: bool,
    properties: ShapeProperties,
}

impl Cylinder {
    /// Creates an uncapped cylinder that extends from negative infinity to positive infinity.
    pub fn new(properties: ShapeProperties) -> Self {
        Cylinder::with_limits(properties, -f64::INFINITY, f64::INFINITY, false)
    }

    /// Creates a new cylinder that extends from the minimum to the maximum limit, not
    /// including the limits.
    pub fn with_limits(
        properties: ShapeProperties,
        minimum: f64,
        maximum: f64,
        closed: bool,
    ) -> Self {
        Self {
            minimum,
            maximum,
            closed,
            properties,
        }
    }

    /// Computes the intersections of the caps.
    fn compute_cap_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersection_recorder: &mut IntersectionRecorder<'a>,
    ) {
        if !self.closed || ray.get_direction().y.abs() <= EPSILON {
            return;
        }

        // Check for intersections with the upper and lower cap by intersecting the with the
        // planes y = minimum and y = maximum.
        [self.minimum, self.maximum]
            .iter()
            .map(|plane| (plane - ray.get_origin().y) / ray.get_direction().y)
            .filter(|t| self.check_cap(ray, *t))
            .for_each(|t| {
                intersection_recorder.add(self, t);
            });
    }

    /// Helper function for the computation of the caps intersections.
    fn check_cap(&self, ray: &Ray, t: f64) -> bool {
        let x = ray.get_origin().x + t * ray.get_direction().x;
        let z = ray.get_origin().z + t * ray.get_direction().z;

        x.powi(2) + z.powi(2) <= 1.
    }
}

impl Shape for Cylinder {}

impl ShapeComputations for Cylinder {
    fn compute_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersection_recorder: &mut IntersectionRecorder<'a>,
    ) {
        let (origin, direction) = (ray.get_origin(), ray.get_direction());
        let a = direction.x.powi(2) + direction.z.powi(2);

        if a.abs() > EPSILON {
            let b = 2. * origin.x * direction.x + 2. * origin.z * direction.z;
            let c = origin.x.powi(2) + origin.z.powi(2) - 1.;
            let disc = b.powi(2) - 4. * a * c;
            if disc < 0. {
                return;
            }

            let disc_sqrt = disc.sqrt();
            let t0 = (-b - disc_sqrt) / (2. * a);
            let t1 = (-b + disc_sqrt) / (2. * a);
            [t0, t1]
                .iter()
                .filter(|&t| {
                    let y = origin.y + t * direction.y;
                    self.minimum < y && y < self.maximum
                })
                .for_each(|&t| intersection_recorder.add(self, t));
        }
        self.compute_cap_intersections(ray, intersection_recorder);
    }

    fn compute_normal(&self, point: &Point4f) -> Vector4f {
        let dist = point.x.powi(2) + point.z.powi(2);
        if dist < 1. && point.y >= (self.maximum - EPSILON) {
            Vector4f::new(0., 1., 0.)
        } else if dist < 1. && point.y <= (self.minimum + EPSILON) {
            Vector4f::new(0., -1., 0.)
        } else {
            Vector4f::new(point.x, 0., point.z)
        }
    }

    fn get_properties(&self) -> &ShapeProperties {
        &self.properties
    }
}

#[cfg(test)]
mod tests {
    use crate::ray::Ray;
    use crate::shapes::shape::ShapeComputations;
    use crate::shapes::{Cylinder, IntersectionRecorder, Shape, ShapeProperties};
    use math::{Point4f, Vector4f};

    /// Asserts the number of intersections for the given rays.
    fn assert_intersections_num(
        shape: &dyn Shape,
        rays: &[Ray],
        expected_intersections_num: &[usize],
    ) {
        rays.iter()
            .zip(expected_intersections_num)
            .for_each(|(ray, expected_num)| {
                let mut intersection_recorder = IntersectionRecorder::new();
                shape.intersect(ray, &mut intersection_recorder);
                assert_eq!(intersection_recorder.len(), *expected_num);
            })
    }

    fn assert_normals(shape: &dyn Shape, points: &[Point4f], expected_normals: &[Vector4f]) {
        points
            .iter()
            .zip(expected_normals)
            .for_each(|(point, expected_normal)| {
                assert_eq!(shape.compute_normal(point), *expected_normal)
            });
    }

    #[test]
    fn compute_intersections() {
        let rays = [
            Ray::new(Point4f::new(1., 0., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0.5, 0., -5.), Vector4f::new(0.1, 1., 1.)),
        ];
        let expected_intersections = [[5., 5.], [4., 6.], [4.80198, 5.]];

        let cylinder = Cylinder::new(ShapeProperties::default());
        rays.iter()
            .zip(expected_intersections)
            .for_each(|(ray, expected_intersections)| {
                let mut intersection_recorder = IntersectionRecorder::new();
                cylinder.intersect(ray, &mut intersection_recorder);

                assert_eq!(intersection_recorder.len(), 2);
                intersection_recorder
                    .sorted()
                    .iter()
                    .map(|intersection| intersection.t)
                    .zip(expected_intersections)
                    .for_each(|(intersection, expected_intersection)| {
                        assert!(math::utils::eq(intersection, expected_intersection));
                    });
            })
    }

    #[test]
    fn compute_intersections_truncated() {
        let rays = [
            Ray::new(Point4f::new(0., 1.5, 0.), Vector4f::new(0.1, 1., 0.)),
            Ray::new(Point4f::new(0., 3., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0., 2., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0., 1., -5.), Vector4f::new(0., 0., 1.)),
            Ray::new(Point4f::new(0., 1.5, -2.), Vector4f::new(0., 0., 1.)),
        ];

        let expected_intersections_num = [0, 0, 0, 0, 0, 2];

        let cylinder = Cylinder::with_limits(ShapeProperties::default(), 1., 2., false);
        assert_intersections_num(&cylinder, &rays, &expected_intersections_num);
    }

    #[test]
    fn compute_intersections_closed() {
        let rays = [
            Ray::new(Point4f::new(0., 3., 0.), Vector4f::new(0., -1., 0.)),
            Ray::new(Point4f::new(0., 3., -2.), Vector4f::new(0., -1., 2.)),
            Ray::new(Point4f::new(0., 4., -2.), Vector4f::new(0., -1., 1.)),
            Ray::new(Point4f::new(0., 0., -2.), Vector4f::new(0., 1., 2.)),
            Ray::new(Point4f::new(0., -1., -2.), Vector4f::new(0., 1., 1.)),
        ];

        let expected_intersections_num = [2; 5];

        let cylinder = Cylinder::with_limits(ShapeProperties::default(), 1., 2., true);
        assert_intersections_num(&cylinder, &rays, &expected_intersections_num);
    }

    #[test]
    fn compute_intersections_miss() {
        let rays = [
            Ray::new(Point4f::new(1., 0., 0.), Vector4f::new(0., 1., 0.)),
            Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 1., 0.)),
            Ray::new(Point4f::new(0., 0., -5.), Vector4f::new(1., 1., 1.)),
        ];

        let cylinder = Cylinder::new(ShapeProperties::default());
        rays.iter().for_each(|ray| {
            let mut intersection_recorder = IntersectionRecorder::new();
            cylinder.intersect(ray, &mut intersection_recorder);
            assert_eq!(intersection_recorder.len(), 0);
        })
    }

    #[test]
    fn compute_normal() {
        let points = [
            Point4f::new(1., 0., 0.),
            Point4f::new(0., 5., -1.),
            Point4f::new(0., -2., 1.),
            Point4f::new(-1., 1., 0.),
        ];

        let expected_normals = [
            Vector4f::new(1., 0., 0.),
            Vector4f::new(0., 0., -1.),
            Vector4f::new(0., 0., 1.),
            Vector4f::new(-1., 0., 0.),
        ];

        let cylinder = Cylinder::new(ShapeProperties::default());
        assert_normals(&cylinder, &points, &expected_normals);
    }

    #[test]
    fn compute_normal_closed() {
        let points = [
            Point4f::new(0., 1., 0.),
            Point4f::new(0.5, 1., 0.),
            Point4f::new(0., 1., 0.5),
            Point4f::new(0., 2., 0.),
            Point4f::new(0.5, 2., 0.),
            Point4f::new(0., 2., 0.5),
        ];

        let expected_normals = [
            Vector4f::new(0., -1., 0.),
            Vector4f::new(0., -1., 0.),
            Vector4f::new(0., -1., 0.),
            Vector4f::new(0., 1., 0.),
            Vector4f::new(0., 1., 0.),
            Vector4f::new(0., 1., 0.),
        ];

        let cylinder = Cylinder::with_limits(ShapeProperties::default(), 1., 2., true);
        assert_normals(&cylinder, &points, &expected_normals);
    }
}
