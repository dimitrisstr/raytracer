use crate::ray::Ray;
use crate::shapes::shape::ShapeComputations;
use crate::shapes::{IntersectionRecorder, Shape, ShapeProperties};
use math::{utils, Point4f, Vector4f};

/// A flat surface that extends infinitely far in x and z dimensions, passing through the origin.
/// The orientation of the plane can be changed, using a transformation matrix.
pub struct Plane {
    properties: ShapeProperties,
}

impl Plane {
    pub fn new(properties: ShapeProperties) -> Self {
        Self { properties }
    }
}

impl Shape for Plane {}

impl ShapeComputations for Plane {
    fn compute_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersections: &mut IntersectionRecorder<'a>,
    ) {
        // The ray is parallel to the plane.
        if utils::eq(ray.get_direction().y, 0.) {
            return;
        }

        intersections.add(self, -ray.get_origin().y / ray.get_direction().y);
    }

    fn compute_normal(&self, point: &Point4f) -> Vector4f {
        Vector4f::new(0., 1., 0.)
    }

    fn get_properties(&self) -> &ShapeProperties {
        &self.properties
    }
}

#[cfg(test)]
mod tests {
    use crate::ray::Ray;
    use crate::shapes::shape::ShapeComputations;
    use crate::shapes::{IntersectionRecorder, Plane, ShapeProperties};
    use math::{Point4f, Vector4f};

    #[test]
    fn compute_normal() {
        let plane = Plane::new(ShapeProperties::default());
        let normal = Vector4f::new(0., 1., 0.);

        assert_eq!(plane.compute_normal(&Point4f::new(0., 0., 0.)), normal);
        assert_eq!(plane.compute_normal(&Point4f::new(10., 0., -10.)), normal);
        assert_eq!(plane.compute_normal(&Point4f::new(-5., 0., 150.)), normal);
    }

    fn compute_intersections(plane: &Plane, ray: Ray) -> IntersectionRecorder {
        let mut intersection_recorder = IntersectionRecorder::new();
        plane.compute_intersections(&ray, &mut intersection_recorder);
        intersection_recorder
    }

    /// Intersect with a ray parallel to the plane.
    #[test]
    fn compute_intersections_parallel_ray() {
        let plane = Plane::new(ShapeProperties::default());
        let ray = Ray::new(Point4f::new(0., 10., 0.), Vector4f::new(0., 0., 1.));

        assert_eq!(compute_intersections(&plane, ray).len(), 0);
    }

    /// Intersect with a coplanar ray.
    #[test]
    fn compute_intersections_coplanar_ray() {
        let plane = Plane::new(ShapeProperties::default());
        let ray = Ray::new(Point4f::new(0., 0., 0.), Vector4f::new(0., 0., 1.));

        assert_eq!(compute_intersections(&plane, ray).len(), 0);
    }

    /// A ray intersecting a plane from above.
    #[test]
    fn compute_intersections_above_ray() {
        let plane = Plane::new(ShapeProperties::default());
        let ray = Ray::new(Point4f::new(0., 1., 0.), Vector4f::new(0., -1., 0.));
        let intersection_recorder = compute_intersections(&plane, ray);

        assert_eq!(intersection_recorder.len(), 1);
        assert_eq!(intersection_recorder.hit().unwrap().t, 1.);
    }

    /// A ray intersecting a plane from below.
    #[test]
    fn compute_intersections_below_ray() {
        let plane = Plane::new(ShapeProperties::default());
        let ray = Ray::new(Point4f::new(0., -1., 0.), Vector4f::new(0., 1., 0.));
        let intersection_recorder = compute_intersections(&plane, ray);

        assert_eq!(intersection_recorder.len(), 1);
        assert_eq!(intersection_recorder.hit().unwrap().t, 1.);
    }
}
