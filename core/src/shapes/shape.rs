use crate::pattern::Pattern;
use crate::properties::{Color, Material};
use crate::ray::Ray;
use math::{Matrix4f, Point4f, Vector4f};

pub trait Shape: ShapeComputations {
    /// Computes the intersections of the ray with the shape.
    fn intersect<'a>(&'a self, ray: &Ray, intersection_recorder: &mut IntersectionRecorder<'a>) {
        let inv_transform = self.get_properties().get_inv_transform();
        let transformed_ray = ray.transform(inv_transform);
        self.compute_intersections(&transformed_ray, intersection_recorder);
    }

    /// Returns the normal of a world point. The normal is a vector that is perpendicular to the point.
    fn normal_at(&self, point: &Point4f) -> Vector4f {
        let object_point = self.get_properties().get_inv_transform() * point;
        let object_normal = self.compute_normal(&object_point);
        let mut world_normal = self.get_properties().get_inv_tp_transform() * &object_normal;
        world_normal.normalize();
        world_normal
    }

    /// Returns the color of a point.
    fn color_at(&self, point: &Point4f) -> Color {
        self.get_properties().color_at(point)
    }

    /// Returns the material of the shape.
    fn get_material(&self) -> &Material {
        self.get_properties().get_material()
    }
}

pub trait ShapeComputations {
    /// Computes the intersections of the transformed ray with the shape.
    fn compute_intersections<'a>(
        &'a self,
        ray: &Ray,
        intersection_recorder: &mut IntersectionRecorder<'a>,
    );

    /// Computes the normal of an object point.
    fn compute_normal(&self, point: &Point4f) -> Vector4f;

    /// Returns the shape's properties.
    fn get_properties(&self) -> &ShapeProperties;
}

/// The properties of a shape.
pub struct ShapeProperties {
    /// The pattern of the shape.
    pattern: Box<dyn Pattern>,
    /// The material of the shape.
    material: Material,
    /// The inverse transformation matrix.
    inv_transform: Matrix4f,
    /// The inverse transpose transformation matrix.
    inv_tp_transform: Matrix4f,
}

impl ShapeProperties {
    /// Creates shape properties.
    pub fn new(pattern: Box<dyn Pattern>, material: Material) -> Self {
        Self {
            pattern,
            material,
            inv_transform: Matrix4f::identity(),
            inv_tp_transform: Matrix4f::identity(),
        }
    }

    /// Creates shape properties with a transformation matrix.
    pub fn with_transform(
        pattern: Box<dyn Pattern>,
        material: Material,
        transform: Matrix4f,
    ) -> Self {
        let inv_transform = transform.inv();
        let inv_tp_transform = inv_transform.transposed();
        Self {
            pattern,
            material,
            inv_transform,
            inv_tp_transform,
        }
    }

    /// Returns the color of the point. The point is in world space.
    pub fn color_at(&self, point: &Point4f) -> Color {
        let object_point = &self.inv_transform * point;
        self.pattern.color_at(&object_point)
    }

    /// Returns the material.
    pub fn get_material(&self) -> &Material {
        &self.material
    }

    /// Returns the inverse transformation matrix.
    pub fn get_inv_transform(&self) -> &Matrix4f {
        &self.inv_transform
    }

    /// Returns the inverse transpose transformation matrix.
    pub fn get_inv_tp_transform(&self) -> &Matrix4f {
        &self.inv_tp_transform
    }
}

impl Default for ShapeProperties {
    fn default() -> Self {
        ShapeProperties::new(Box::new(Color::from((255, 255, 255))), Material::default())
    }
}

/// Stores the information of an intersection.
#[derive(Copy, Clone)]
pub struct Intersection<'a> {
    /// The material of the intersected object.
    pub shape: &'a dyn Shape,
    /// The distance of the intersection.
    pub t: f64,
}

/// Stores intersections and calculates the hit intersection.
pub struct IntersectionRecorder<'a> {
    /// The smallest positive intersection.
    hit: Option<Intersection<'a>>,
    /// All the intersections.
    intersections: Vec<Intersection<'a>>,
}

impl<'a> IntersectionRecorder<'a> {
    /// Creates a new intersection recorder.
    pub fn new() -> Self {
        Self {
            hit: None,
            intersections: Vec::new(),
        }
    }

    /// Adds an intersection.
    pub fn add(&mut self, shape: &'a dyn Shape, t: f64) {
        let intersection = Intersection { shape, t };
        self.intersections.push(intersection);
        if t.is_sign_positive() && (self.hit.is_none() || t < self.hit.as_ref().unwrap().t) {
            self.hit = Some(intersection);
        }
    }

    /// Returns the number of intersections.
    pub fn len(&self) -> usize {
        self.intersections.len()
    }

    /// Returns a vector with all the intersections in ascending order.
    pub fn sorted(&self) -> Vec<Intersection<'a>> {
        let mut sorted_vector = self.intersections.clone();
        sorted_vector.sort_by(|a, b| a.t.partial_cmp(&b.t).unwrap());
        sorted_vector
    }

    /// Returns the smallest positive intersection.
    pub fn hit(&self) -> Option<Intersection<'a>> {
        self.hit
    }
}

#[cfg(test)]
mod tests {
    mod intersection {
        use crate::shapes::{IntersectionRecorder, Sphere};

        #[test]
        fn add() {
            let sphere1 = Sphere::default();
            let sphere2 = Sphere::default();

            let mut intersection_recorder = IntersectionRecorder::new();
            intersection_recorder.add(&sphere1, 0.5);
            intersection_recorder.add(&sphere2, 2.5);

            assert_eq!(intersection_recorder.intersections[0].t, 0.5);
            assert!(std::ptr::eq(
                intersection_recorder.intersections[0].shape,
                &sphere1
            ));
            assert_eq!(intersection_recorder.intersections[1].t, 2.5);
            assert!(std::ptr::eq(
                intersection_recorder.intersections[1].shape,
                &sphere2
            ));
            assert_eq!(intersection_recorder.len(), 2);
        }

        #[test]
        fn hit() {
            let sphere1 = Sphere::default();
            let sphere2 = Sphere::default();
            let sphere3 = Sphere::default();

            let mut intersection_recorder = IntersectionRecorder::new();
            intersection_recorder.add(&sphere1, 10.);
            intersection_recorder.add(&sphere2, -1.);
            intersection_recorder.add(&sphere3, 2.2);

            assert_eq!(intersection_recorder.hit.unwrap().t, 2.2);
            assert!(std::ptr::eq(
                intersection_recorder.hit.unwrap().shape,
                &sphere3
            ));
        }

        /// An intersection recorder that only has negative intersections.
        #[test]
        fn hit_none() {
            let sphere1 = Sphere::default();
            let sphere2 = Sphere::default();

            let mut intersection_recorder = IntersectionRecorder::new();
            intersection_recorder.add(&sphere1, -1.);
            intersection_recorder.add(&sphere2, -3.);

            assert_eq!(intersection_recorder.len(), 2);
            assert!(intersection_recorder.hit().is_none());
        }
    }
}
