//! # Math
//!
//! This crate implements four-dimensional points, vectors and matrices.

pub use crate::point::Point4f;
pub use crate::vector::Vector4f;

pub type Matrix4f = crate::Matrixf<4>;
pub type Matrix3f = crate::Matrixf<3>;
pub type Matrix2f = crate::Matrixf<2>;

mod point {
    use crate::utils;
    use crate::vector::Vector4f;

    /// A four-dimensional Point.
    #[derive(Clone, Debug)]
    pub struct Point4f {
        pub x: f64,
        pub y: f64,
        pub z: f64,
        pub w: f64,
    }

    impl Point4f {
        /// Creates a four-dimensional point. The fourth dimension is 1.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let a = Point4f::new(1., 2., 3.);
        ///
        /// assert_eq!(a, Point4f{x: 1.0, y: 2.0, z: 3., w: 1.});
        /// ```
        pub fn new(x: f64, y: f64, z: f64) -> Self {
            Self { x, y, z, w: 1. }
        }
    }

    impl PartialEq for Point4f {
        fn eq(&self, other: &Self) -> bool {
            utils::eq(self.x, other.x)
                && utils::eq(self.y, other.y)
                && utils::eq(self.z, other.z)
                && utils::eq(self.w, other.w)
        }
    }

    impl std::ops::Add<&Vector4f> for &Point4f {
        type Output = Point4f;

        /// Adds a vector to a point and returns a new point.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let a = Point4f::new(3., 2., 1.);
        /// let b = Vector4f::new(5., 6., 7.);
        ///
        /// assert_eq!(&a + &b, Point4f::new(8., 8., 8.));
        ///```
        fn add(self, rhs: &Vector4f) -> Self::Output {
            let mut point = self.clone();
            point += rhs;
            point
        }
    }

    impl std::ops::AddAssign<&Vector4f> for Point4f {
        /// Adds a vector to a point.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let mut a = Point4f::new(3., 2., 1.);
        /// let b = Vector4f::new(5., 6., 7.);
        /// a += &b;
        ///
        /// assert_eq!(a, Point4f::new(8., 8., 8.));
        ///```
        fn add_assign(&mut self, rhs: &Vector4f) {
            self.x += rhs.x;
            self.y += rhs.y;
            self.z += rhs.z;
            self.w += rhs.w;
        }
    }

    impl std::ops::Sub<&Vector4f> for &Point4f {
        type Output = Point4f;

        /// Subtracts a vector from a point and returns a new point.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let a = Point4f::new(3., 4., 5.);
        /// let b = Vector4f::new(1., 2., 3.);
        ///
        /// assert_eq!(&a - &b, Point4f::new(2., 2., 2.));
        /// ```
        fn sub(self, rhs: &Vector4f) -> Self::Output {
            let mut point = self.clone();
            point -= rhs;
            point
        }
    }

    impl std::ops::SubAssign<&Vector4f> for Point4f {
        /// Subtracts a vector from a point.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let mut a = Point4f::new(3., 4., 5.);
        /// let b = Vector4f::new(1., 2., 3.);
        /// a -= &b;
        ///
        /// assert_eq!(a, Point4f::new(2., 2., 2.));
        /// ```
        fn sub_assign(&mut self, rhs: &Vector4f) {
            self.x -= rhs.x;
            self.y -= rhs.y;
            self.z -= rhs.z;
            self.w -= rhs.w;
        }
    }

    impl std::ops::Sub<&Point4f> for &Point4f {
        type Output = Vector4f;

        /// Subtracts two points and returns a vector.
        ///
        /// ```
        /// # use math::{Point4f, Vector4f};
        /// let a = Point4f::new(3., 2., 1.);
        /// let b = Point4f::new(5., 6., 7.);
        ///
        /// assert_eq!(&a - &b, Vector4f::new(-2., -4., -6.));
        /// ```
        fn sub(self, rhs: &Point4f) -> Self::Output {
            Vector4f {
                x: self.x - rhs.x,
                y: self.y - rhs.y,
                z: self.z - rhs.z,
                w: self.w - rhs.w,
            }
        }
    }
}

mod vector {
    use crate::utils;

    /// A four-dimensional Vector.
    #[derive(Clone, Debug)]
    pub struct Vector4f {
        pub x: f64,
        pub y: f64,
        pub z: f64,
        pub w: f64,
    }

    impl Vector4f {
        /// Creates a four-dimensional Vector. The fourth dimension is 0.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        ///
        /// assert_eq!(a, Vector4f{x: 1.0, y: 2.0, z: 3., w: 0.});
        /// ```
        pub fn new(x: f64, y: f64, z: f64) -> Self {
            Self { x, y, z, w: 0. }
        }

        /// Calculates the dot product of two vectors.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        /// let b = Vector4f::new(2., 3., 4.);
        ///
        /// assert_eq!(a.dot(&b), 20.);
        /// ```
        pub fn dot(&self, other: &Self) -> f64 {
            self.x * other.x + self.y * other.y + self.z * other.z + self.w * other.w
        }

        /// Calculates the three-dimensional cross product.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        /// let b = Vector4f::new(2., 3., 4.);
        ///
        /// assert_eq!(a.cross(&b), Vector4f::new(-1., 2., -1.));
        /// assert_eq!(b.cross(&a), Vector4f::new(1., -2., 1.));
        /// ```
        pub fn cross(&self, other: &Self) -> Self {
            Vector4f::new(
                self.y * other.z - self.z * other.y,
                self.z * other.x - self.x * other.z,
                self.x * other.y - self.y * other.x,
            )
        }

        /// Calculates the length of the vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        ///
        /// assert_eq!(a.length(), (14 as f64).sqrt());
        /// assert_eq!((-&a).length(), (14 as f64).sqrt());
        /// ```
        pub fn length(&self) -> f64 {
            self.dot(self).sqrt()
        }

        /// Normalizes the vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let mut a = Vector4f::new(1., 2., 3.);
        /// a.normalize();
        ///
        /// assert_eq!(a, Vector4f::new(0.26726, 0.53452, 0.80178));
        /// ```
        pub fn normalize(&mut self) {
            let length = self.length();
            self.x /= length;
            self.y /= length;
            self.z /= length;
            self.w /= length;
        }

        /// Returns a new normalized vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        ///
        /// assert_eq!(a.normalized(), Vector4f::new(0.26726, 0.53452, 0.80178));
        /// ```
        pub fn normalized(&self) -> Self {
            let mut vec_normalized = self.clone();
            vec_normalized.normalize();
            vec_normalized
        }

        /// Returns the reflection of the vector around the normal vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let vector = Vector4f::new(1., -1., 0.);
        /// let normal = Vector4f::new(0., 1., 0.);
        ///
        /// assert_eq!(vector.reflect(&normal), Vector4f::new(1., 1., 0.));
        ///
        /// ```
        pub fn reflect(&self, normal: &Vector4f) -> Self {
            self - &(normal * (2. * self.dot(normal)))
        }
    }

    impl PartialEq for Vector4f {
        fn eq(&self, other: &Self) -> bool {
            utils::eq(self.x, other.x)
                && utils::eq(self.y, other.y)
                && utils::eq(self.z, other.z)
                && utils::eq(self.w, other.w)
        }
    }

    impl std::ops::Add<&Vector4f> for &Vector4f {
        type Output = Vector4f;

        /// Adds two vectors and returns a new vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        /// let b = Vector4f::new(2., 3., 4.);        
        ///
        /// assert_eq!(&a + &b, Vector4f::new(3., 5., 7.));
        /// ```
        fn add(self, rhs: &Vector4f) -> Self::Output {
            let mut vec = self.clone();
            vec += rhs;
            vec
        }
    }

    impl std::ops::AddAssign<&Vector4f> for Vector4f {
        /// Adds a vector to a vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let mut a = Vector4f::new(1., 2., 3.);       
        /// let b = Vector4f::new(2., 3., 4.);
        /// a += &b;
        ///
        /// assert_eq!(a, Vector4f::new(3., 5., 7.));
        /// ```
        fn add_assign(&mut self, rhs: &Vector4f) {
            self.x += rhs.x;
            self.y += rhs.y;
            self.z += rhs.z;
            self.w += rhs.w;
        }
    }

    impl std::ops::Sub<&Vector4f> for &Vector4f {
        type Output = Vector4f;

        /// Subtracts two vectors and returns a new vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., 2., 3.);
        /// let b = Vector4f::new(4., 8., 1.);
        ///
        /// assert_eq!(&a - &b, Vector4f::new(-3., -6., 2.));
        /// ```
        fn sub(self, rhs: &Vector4f) -> Self::Output {
            let mut vec = self.clone();
            vec -= rhs;
            vec
        }
    }

    impl std::ops::SubAssign<&Vector4f> for Vector4f {
        /// Subtracts a vector from a vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let mut a = Vector4f::new(1., 2., 3.);
        /// let b = Vector4f::new(4., 8., 1.);
        /// a -= &b;
        ///
        /// assert_eq!(a, Vector4f::new(-3., -6., 2.));
        /// ```
        fn sub_assign(&mut self, rhs: &Vector4f) {
            self.x -= rhs.x;
            self.y -= rhs.y;
            self.z -= rhs.z;
            self.w -= rhs.w;
        }
    }

    impl std::ops::Neg for &Vector4f {
        type Output = Vector4f;

        /// Returns a new negated vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// assert_eq!(-&Vector4f::new(1., 2., 3.), Vector4f::new(-1., -2., -3.));
        /// ```
        fn neg(self) -> Self::Output {
            let mut vec = self.clone();
            vec.x = -self.x;
            vec.y = -self.y;
            vec.z = -self.z;
            vec.w = -self.w;
            vec
        }
    }

    impl std::ops::Mul<f64> for &Vector4f {
        type Output = Vector4f;

        /// Multiplies a vector by a scalar and returns a new vector.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let a = Vector4f::new(1., -2., 3.);
        ///
        /// assert_eq!(&a * 3.5, Vector4f::new(3.5, -7., 10.5));
        /// assert_eq!(&a * 0.5, Vector4f::new(0.5, -1., 1.5));
        /// ```
        fn mul(self, rhs: f64) -> Self::Output {
            let mut vec = self.clone();
            vec *= rhs;
            vec
        }
    }

    impl std::ops::MulAssign<f64> for Vector4f {
        /// Multiplies a vector by a scalar.
        ///
        /// ```
        /// # use math::Vector4f;
        /// let mut a = Vector4f::new(1., -2., 3.);
        /// a *= 0.5;
        ///
        /// assert_eq!(a, Vector4f::new(0.5, -1., 1.5));
        /// ```
        fn mul_assign(&mut self, rhs: f64) {
            self.x *= rhs;
            self.y *= rhs;
            self.z *= rhs;
            self.w *= rhs;
        }
    }
}

/// A D x D matrix.
#[derive(Clone, Debug)]
pub struct Matrixf<const D: usize>([[f64; D]; D]);

impl<const D: usize> Matrixf<D> {
    /// Creates a D x D matrix, initialized with zeros.
    pub fn new() -> Matrixf<D> {
        Self([[0.; D]; D])
    }

    /// Transposes the matrix.
    ///
    /// ```
    /// # use math::Matrix4f;
    /// let mut a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [5., 6., 7., 8.],
    ///     [9., 10., 11., 12.],
    ///     [13., 14., 15., 16.],
    /// ]);
    /// a.transpose();
    ///
    /// assert_eq!(
    ///     a,
    ///     Matrix4f::from([
    ///         [1., 5., 9., 13.],
    ///         [2., 6., 10., 14.],
    ///         [3., 7., 11., 15.],
    ///         [4., 8., 12., 16.]
    ///     ])
    /// );
    /// ```
    pub fn transpose(&mut self) {
        for i in 1..D {
            for j in i..D {
                // Swap values.
                (self.0[j][i - 1], self.0[i - 1][j]) = (self.0[i - 1][j], self.0[j][i - 1]);
            }
        }
    }

    /// Creates a new transposed matrix.
    ///
    /// ```
    /// # use math::Matrix4f;
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [5., 6., 7., 8.],
    ///     [9., 10., 11., 12.],
    ///     [13., 14., 15., 16.],
    /// ]);
    ///
    /// assert_eq!(
    ///     a.transposed(),
    ///     Matrix4f::from([
    ///         [1., 5., 9., 13.],
    ///         [2., 6., 10., 14.],
    ///         [3., 7., 11., 15.],
    ///         [4., 8., 12., 16.]
    ///     ])
    /// );
    /// ```
    pub fn transposed(&self) -> Matrixf<D> {
        let mut transposed_matrix = self.clone();
        transposed_matrix.transpose();
        transposed_matrix
    }

    /// Creates a submatrix by deleting one row and one column. The indexes, row and col, start
    /// from 0.
    ///
    /// ```
    /// # use math::{Matrix3f, Matrix4f};
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [5., 6., 7., 8.],
    ///     [9., 10., 11., 12.],
    ///     [13., 14., 15., 16.],
    /// ]);
    /// let b: Matrix3f = a.submatrix(1, 2);
    ///
    /// assert_eq!(b, Matrix3f::from([[1., 2., 4.], [9., 10., 12.], [13., 14., 16.]]));
    /// ```
    ///
    /// # Panics
    ///
    /// Panics if the submatrix dimension isn't D - 1.
    ///
    /// ```should_panic
    /// # use math::{Matrix2f, Matrix3f, Matrix4f};
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [5., 6., 7., 8.],
    ///     [9., 10., 11., 12.],
    ///     [13., 14., 15., 16.],
    /// ]);
    ///
    /// let b: Matrix2f = a.submatrix(1, 3);
    /// ```
    pub fn submatrix<const S: usize>(&self, row: usize, col: usize) -> Matrixf<S> {
        if S != D - 1 {
            panic!("Wrong submatrix dimension.");
        };

        let mut matrix = Matrixf::<S>::new();

        let mut current_row = 0;
        for row_index in 0..D {
            if row_index != row {
                let mut current_col = 0;
                for col_index in 0..D {
                    if col_index != col {
                        matrix.0[current_row][current_col] = self.0[row_index][col_index];
                        current_col += 1;
                    }
                }
                current_row += 1;
            }
        }
        matrix
    }
}

impl<const D: usize> From<[[f64; D]; D]> for Matrixf<D> {
    /// Converts a two dimensional array to a matrix.
    fn from(array: [[f64; D]; D]) -> Self {
        Self(array)
    }
}

impl<const D: usize> std::ops::Mul<&Matrixf<D>> for &Matrixf<D> {
    type Output = Matrixf<D>;

    /// Multiplies two matrices.
    ///
    /// ```
    /// # use math::{Matrix4f, Matrixf};
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [5., 6., 7., 8.],
    ///     [9., 8., 7., 6.],
    ///     [5., 4., 3., 2.],
    /// ]);
    ///
    /// let b = Matrix4f::from([
    ///     [-2., 1., 2., 3.],
    ///     [3., 2., 1., -1.],
    ///     [4., 3., 6., 5.],
    ///     [1., 2., 7., 8.],
    /// ]);
    ///
    /// assert_eq!(
    ///     &a * &b,
    ///     Matrix4f::from([
    ///         [20., 22., 50., 48.],
    ///         [44., 54., 114., 108.],
    ///         [40., 58., 110., 102.],
    ///         [16., 26., 46., 42.]
    ///     ])
    /// );
    /// ```
    fn mul(self, rhs: &Matrixf<D>) -> Self::Output {
        let mut matrix = Matrixf::new();

        for row in 0..D {
            for col in 0..D {
                for index in 0..D {
                    matrix.0[row][col] += self.0[row][index] * rhs.0[index][col];
                }
            }
        }

        matrix
    }
}

impl<const D: usize> PartialEq for Matrixf<D> {
    fn eq(&self, other: &Self) -> bool {
        for row in 0..D {
            for col in 0..D {
                if !utils::eq(self.0[row][col], other.0[row][col]) {
                    return false;
                }
            }
        }
        true
    }
}

impl Matrix2f {
    /// Calculates the determinant of the matrix.
    fn determinant(&self) -> f64 {
        self.0[0][0] * self.0[1][1] - self.0[0][1] * self.0[1][0]
    }
}

impl Matrix3f {
    /// Calculates the determinant of the matrix.
    fn determinant(&self) -> f64 {
        self.0[0][0] * self.cofactor(0, 0)
            + self.0[0][1] * self.cofactor(0, 1)
            + self.0[0][2] * self.cofactor(0, 2)
    }

    /// Calculates the minor of the matrix.
    fn minor(&self, row: usize, col: usize) -> f64 {
        self.submatrix::<2>(row, col).determinant()
    }

    /// Calculates the cofactor of the matrix.
    fn cofactor(&self, row: usize, col: usize) -> f64 {
        let minor = self.minor(row, col);

        if (row + col) % 2 != 0 {
            -minor
        } else {
            minor
        }
    }
}

impl Matrix4f {
    /// Calculates the determinant of the matrix.
    fn determinant(&self) -> f64 {
        self.0[0][0] * self.cofactor(0, 0)
            + self.0[0][1] * self.cofactor(0, 1)
            + self.0[0][2] * self.cofactor(0, 2)
            + self.0[0][3] * self.cofactor(0, 3)
    }

    /// Calculates the minor of the matrix.
    fn minor(&self, row: usize, col: usize) -> f64 {
        self.submatrix::<3>(row, col).determinant()
    }

    /// Calculates the cofactor of the matrix.
    fn cofactor(&self, row: usize, col: usize) -> f64 {
        let minor = self.minor(row, col);

        if (row + col) % 2 != 0 {
            -minor
        } else {
            minor
        }
    }

    /// Calculates the inverse of the matrix.
    ///
    /// ```
    /// # use math::Matrix4f;
    /// let a = Matrix4f::from([
    ///     [8., -5., 9., 2.],
    ///     [7., 5., 6., 1.],
    ///     [-6., 0., 9., 6.],
    ///     [-3., 0., -9., -4.],
    /// ]);
    ///
    /// assert_eq!(
    ///     a.inv(),
    ///     Matrix4f::from([
    ///         [-0.15385, -0.15385, -0.28205, -0.53846],
    ///         [-0.07692, 0.12308, 0.02564, 0.03077],
    ///         [0.35897, 0.35897, 0.43590, 0.92308],
    ///         [-0.69231, -0.69231, -0.76923, -1.92308]
    ///     ])
    /// );
    /// ```
    pub fn inv(&self) -> Matrix4f {
        let det = self.determinant();
        assert!(det != 0.);

        let mut mat = Matrix4f::new();
        for row in 0..4 {
            for col in 0..4 {
                let c = self.cofactor(row, col);
                mat.0[col][row] = c / det;
            }
        }
        mat
    }

    /// Creates an identity matrix. This matrix is used as a starting point for transformation
    /// chains.
    ///
    /// # Examples
    ///
    /// ```
    /// # use math::Matrix4f;
    /// let transform = Matrix4f::identity()
    ///     .rotate_x(std::f64::consts::PI / 2.)
    ///     .scale(5., 4., 3.)
    ///     .translate(10., -4., 7.);
    /// ```
    pub fn identity() -> Matrix4f {
        Self([
            [1., 0., 0., 0.],
            [0., 1., 0., 0.],
            [0., 0., 1., 0.],
            [0., 0., 0., 1.],
        ])
    }

    /// Multiplies the matrix by a translation transformation matrix.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f, Vector4f};
    /// let transform = Matrix4f::identity().translate(5., -3., 2.);
    /// let point = Point4f::new(-3., 4., 5.);
    /// let vector = Vector4f::new(-3., 4., 5.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(2., 1., 7.));
    /// assert_eq!(&transform * &vector, vector);
    /// ```
    pub fn translate(&self, x: f64, y: f64, z: f64) -> Matrix4f {
        &Self([
            [1., 0., 0., x],
            [0., 1., 0., y],
            [0., 0., 1., z],
            [0., 0., 0., 1.],
        ]) * self
    }

    /// Multiplies the matrix by a scale transformation matrix.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f, Vector4f};
    /// let transform = Matrix4f::identity().scale(2., 3., 4.);
    /// let point = Point4f::new(-4., 6., 8.);
    /// let vector = Vector4f::new(-4., 6., 8.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(-8., 18., 32.));
    /// assert_eq!(&transform * &vector, Vector4f::new(-8., 18., 32.));
    /// ```
    pub fn scale(&self, x: f64, y: f64, z: f64) -> Matrix4f {
        &Self([
            [x, 0., 0., 0.],
            [0., y, 0., 0.],
            [0., 0., z, 0.],
            [0., 0., 0., 1.],
        ]) * self
    }

    /// Multiplies the matrix by a x axis rotation transformation matrix. The angle is in radians.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f};
    /// let transform = Matrix4f::identity().rotate_x(std::f64::consts::PI / 2.);
    /// let point = Point4f::new(0., 1., 0.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(0., 0., 1.));
    /// ```
    #[rustfmt::skip]
    pub fn rotate_x(&self, angle: f64) -> Matrix4f {
        &Self([
            [1.,          0.,           0., 0.],
            [0., angle.cos(), -angle.sin(), 0.],
            [0., angle.sin(),  angle.cos(), 0.],
            [0.,          0.,           0., 1.],
        ]) * self
    }

    /// Multiplies the matrix by a y axis rotation transformation matrix. The angle is in radians.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f};
    /// let transform = Matrix4f::identity().rotate_y(std::f64::consts::PI / 2.);
    /// let point = Point4f::new(0., 0., 1.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(1., 0., 0.));
    /// ```
    #[rustfmt::skip]
    pub fn rotate_y(&self, angle: f64) -> Matrix4f {
        &Self([
            [ angle.cos(), 0.,  angle.sin(), 0.],
            [          0., 1.,           0., 0.],
            [-angle.sin(), 0.,  angle.cos(), 0.],
            [          0., 0.,           0., 1.],
        ]) * self
    }

    /// Multiplies the matrix by a z axis rotation transformation matrix. The angle is in radians.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f};
    /// let transform = Matrix4f::identity().rotate_z(std::f64::consts::PI / 2.);
    /// let point = Point4f::new(0., 1., 0.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(-1., 0., 0.));
    /// ```
    #[rustfmt::skip]
    pub fn rotate_z(&self, angle: f64) -> Matrix4f {
        &Self([
            [angle.cos(), -angle.sin(), 0., 0.],
            [angle.sin(),  angle.cos(), 0., 0.],
            [         0.,           0., 1., 0.],
            [         0.,           0., 0., 1.],
        ]) * self
    }

    /// Multiplies the matrix by a shearing transformation matrix.
    ///
    /// *This method can be used in transformation chains.*
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f};
    /// let transform = Matrix4f::identity().shearing(1., 0., 0., 0., 0., 0.);
    /// let point = Point4f::new(2., 3., 4.);
    ///
    /// assert_eq!(&transform * &point, Point4f::new(5., 3., 4.));
    /// ```
    pub fn shearing(&self, xy: f64, xz: f64, yx: f64, yz: f64, zx: f64, zy: f64) -> Matrix4f {
        &Self([
            [1., xy, xz, 0.],
            [yx, 1., yz, 0.],
            [zx, zy, 1., 0.],
            [0., 0., 0., 1.],
        ]) * self
    }
}

impl std::ops::Mul<&Vector4f> for &Matrix4f {
    type Output = Vector4f;

    /// Multiplies a matrix by a vector.
    ///
    /// ```
    /// # use math::{Matrix4f, Vector4f};
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [2., 4., 4., 2.],
    ///     [8., 6., 4., 1.],
    ///     [0., 0., 0., 1.],
    /// ]);
    /// let vector = Vector4f::new(1., 2., 3.);
    ///
    /// assert_eq!(&a * &vector, Vector4f::new(14., 22., 32.));
    /// ```
    fn mul(self, rhs: &Vector4f) -> Self::Output {
        let mut vector = [0.; 4];
        for (row_index, row) in self.0.iter().enumerate() {
            vector[row_index] += row[0] * rhs.x + row[1] * rhs.y + row[2] * rhs.z + row[3] * rhs.w;
        }
        Vector4f::new(vector[0], vector[1], vector[2])
    }
}

impl std::ops::Mul<&Point4f> for &Matrix4f {
    type Output = Point4f;

    /// Multiplies a matrix by a point.
    ///
    /// ```
    /// # use math::{Matrix4f, Point4f};
    /// let a = Matrix4f::from([
    ///     [1., 2., 3., 4.],
    ///     [2., 4., 4., 2.],
    ///     [8., 6., 4., 1.],
    ///     [0., 0., 0., 1.],
    /// ]);
    /// let point = Point4f::new(1., 2., 3.);
    ///
    /// assert_eq!(&a * &point, Point4f::new(18., 24., 33.));
    /// ```
    fn mul(self, rhs: &Point4f) -> Self::Output {
        let mut point = [0.; 4];
        for (row_index, row) in self.0.iter().enumerate() {
            point[row_index] += row[0] * rhs.x + row[1] * rhs.y + row[2] * rhs.z + row[3] * rhs.w;
        }
        Point4f::new(point[0], point[1], point[2])
    }
}

pub mod utils {
    pub const EPSILON: f64 = 0.00001;

    /// Checks if the difference of two values is less than a constant EPISLON.
    pub fn eq(n1: f64, n2: f64) -> bool {
        (n1 - n2).abs() <= EPSILON
    }
}

#[cfg(test)]
mod tests {

    mod matrix2f {
        use crate::Matrix2f;
        #[test]
        fn determinant() {
            let a = Matrix2f::from([[1., 5.], [-3., 2.]]);

            assert_eq!(a.determinant(), 17.);
        }
    }

    mod matrix3f {
        use crate::{Matrix2f, Matrix3f};

        #[test]
        fn submatrix() {
            let a = Matrix3f::from([[1., 5., 0.], [-3., 2., 7.], [0., 6., -3.]]);

            assert_eq!(a.submatrix(0, 2), Matrix2f::from([[-3., 2.], [0., 6.]]));
        }

        #[test]
        fn minor() {
            let a = Matrix3f::from([[3., 5., 0.], [2., -1., -7.], [6., -1., 5.]]);

            assert_eq!(a.minor(0, 0), -12.);
            assert_eq!(a.minor(1, 0), 25.);
        }

        #[test]
        fn cofactor() {
            let a = Matrix3f::from([[3., 5., 0.], [2., -1., -7.], [6., -1., 5.]]);
            let cofactors = [[-12., -52., 4.], [-25., 15., 33.], [-35., 21., -13.]];

            for row in 0..3 {
                for col in 0..3 {
                    assert_eq!(a.cofactor(row, col), cofactors[row][col]);
                }
            }
        }

        #[test]
        fn determinant() {
            let a = Matrix3f::from([[1., 2., 6.], [-5., 8., -4.], [2., 6., 4.]]);

            assert_eq!(a.determinant(), -196.);
        }
    }

    mod matrix4f {
        use crate::{Matrix4f, Point4f};

        #[test]
        fn cofactor() {
            let a = Matrix4f::from([
                [-5., 2., 6., -8.],
                [1., -5., 1., 8.],
                [7., 7., -6., -7.],
                [1., -3., 7., 4.],
            ]);

            let cofactors = [
                [116., -430., -42., -278.],
                [240., -775., -119., -433.],
                [128., -236., -28., -160.],
                [-24., 277., 105., 163.],
            ];

            for row in 0..4 {
                for col in 0..4 {
                    assert_eq!(a.cofactor(row, col), cofactors[row][col]);
                }
            }
        }

        #[test]
        fn determinant() {
            let a = Matrix4f::from([
                [-2., -8., 3., 5.],
                [-3., 1., 7., 3.],
                [1., 2., -9., 6.],
                [-6., 7., 7., -9.],
            ]);

            assert_eq!(a.determinant(), -4071.);
        }

        #[test]
        fn inv() {
            let a = Matrix4f::from([
                [-5., 2., 6., -8.],
                [1., -5., 1., 8.],
                [7., 7., -6., -7.],
                [1., -3., 7., 4.],
            ]);

            assert_eq!(
                a.inv(),
                Matrix4f::from([
                    [0.21805, 0.45113, 0.24060, -0.04511],
                    [-0.80827, -1.45677, -0.44361, 0.52068],
                    [-0.07895, -0.22368, -0.05263, 0.19737],
                    [-0.52256, -0.81391, -0.30075, 0.30639]
                ])
            );
        }

        #[test]
        fn shearing() {
            let point = Point4f::new(2., 3., 4.);
            let transform = Matrix4f::identity().shearing(1., 0., 0., 0., 0., 0.);
            assert_eq!(&transform * &point, Point4f::new(5., 3., 4.));

            let transform = Matrix4f::identity().shearing(0., 1., 0., 0., 0., 0.);
            assert_eq!(&transform * &point, Point4f::new(6., 3., 4.));

            let transform = Matrix4f::identity().shearing(0., 0., 1., 0., 0., 0.);
            assert_eq!(&transform * &point, Point4f::new(2., 5., 4.));

            let transform = Matrix4f::identity().shearing(0., 0., 0., 1., 0., 0.);
            assert_eq!(&transform * &point, Point4f::new(2., 7., 4.));

            let transform = Matrix4f::identity().shearing(0., 0., 0., 0., 1., 0.);
            assert_eq!(&transform * &point, Point4f::new(2., 3., 6.));

            let transform = Matrix4f::identity().shearing(0., 0., 0., 0., 0., 1.);
            assert_eq!(&transform * &point, Point4f::new(2., 3., 7.));
        }
    }
}
